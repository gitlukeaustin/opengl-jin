//
// Created by trotfunky on 07/10/2019.
//

#ifndef TESTS_OPENGL_INPUTSTATUS_H
#define TESTS_OPENGL_INPUTSTATUS_H

#include <map>
#include <iostream>
//#include <GLFW/glfw3.h>

#include <GL/glut.h>
#include <GL/freeglut_std.h>

#include "Vectors.h"


/// Handles the key and mouse events from glut and keep their status up to date.
/// "Static class"
class InputStatus {
public:
    InputStatus() = delete;

    static void register_glut_callbacks();

    // FIXME: Not happy with having two functions : char auto promoted to int if same name
    static bool is_key_pressed(unsigned char key);
    static bool is_special_key_pressed(int key);

    static bool is_mouse_button_pressed(int mouse_button);

    /// Updates and returns the movement of the mouse.
    /// \param update If true, updates the mouse delta that will be returned
    /// \return Updated displacement on each axis between the two last updates.
    static const Vec2i& get_mouse_delta(bool update = false);

    // Glut callbacks for input events
    static void key_press(unsigned char event_key, int mouse_x, int mouse_y);
    static void key_up(unsigned char event_key, int mouse_x, int mouse_y);
    static void special_key_press(int event_key, int mouse_x, int mouse_y);
    static void special_key_up(int event_key, int mouse_x, int mouse_y);

    static void mouse_click(int mouse_button, int button_state, int mouse_x, int mouse_y);
    /// Accumulates the movements of the mouse
    static void mouse_movement(int mouse_x, int mouse_y);

    static Vec2i window_size;

    static Vec2f mouse_sensitivity;
private:
    // The maps are used to keep track of the keys which were pressed or released during runtime.
    static std::map<unsigned char,bool> ascii_keys_status;
    static std::map<int,bool> special_keys_status;
    static std::map<int,bool> mouse_button_status;

    // Updated by callback
    static Vec2i current_mouse_delta;
    // Updated on user request
    static Vec2i last_mouse_delta;
    static Vec2i last_mouse_position;

    static constexpr int cursor_bounds_width = 5;

    // These functions are called by *_press and *_up to update their values
    static void update_key(unsigned char event_key, bool new_status);
    static void update_special_key(int event_key, bool new_status);

    static void keep_cursor_bounded(int mouse_x, int mouse_y);
};


#endif //TESTS_OPENGL_INPUTSTATUS_H
