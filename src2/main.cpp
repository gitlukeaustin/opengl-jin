#include <GLFW/glfw3.h>
#include <GL/glut.h>
#include <GL/gl.h>

#include "displayers.h"
#include "DataHandling/Texture.h"
#include "DataHandling/Model3D.h"
#include "InputStatus.h"
#include "Camera.h"

#define PI 3.14159265
#define LIFETIME 500
#define COLOR_SEP 50

volatile unsigned long long int timer_ticks = 0;
static double aspect_ratio;
static Texture tree_texture;
static Model3D raptor;
static Model3D bunny;
static Texture raptor_texture;
static Texture bunny_texture;
static Camera camera({100,3,100},{-10,0,-10},{0,1,0});
static float raptor_angle;
static float bunny_angle;
static Vec3d bunny_pos;
static int timer = 0;
static bool time_flow = true;
static Texture terrain_heightmap;
static Texture terrain_texture;

GLFWwindow *window;

void draw_heightmap(Texture heightmap, Texture texture)
{
    uint8_t channels = heightmap.color_bits/8;
    glBindTexture(GL_TEXTURE_2D, texture.opengl_id[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);    // on répète la texture en cas de U,V > 1.0
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);    // ou < 0.0
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); // indique qu'il faut mélanger la texture avec la couleur courante

    // charge le tableau de la texture en mémoire vidéo et crée une texture mipmap
    if (texture.color_bits == 32)
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture.width, texture.height, GL_RGBA, GL_UNSIGNED_BYTE, texture.image_data);
    else
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture.width, texture.height, GL_RGB, GL_UNSIGNED_BYTE, texture.image_data);

    glBegin(GL_TRIANGLES);

    for (int i = 0 ; i < heightmap.height - 1; i++)
    {
        for (int j = 0 ; j < heightmap.width - 1; j++)
        {
            float height[] = {0,0,0,0};
            for (int c = 0 ; c < channels ; c++) {
                height[0] += heightmap.image_data[(i * heightmap.height + j) * channels + c] * (COLOR_SEP * (channels-c))/10000;
                height[1] += heightmap.image_data[(i * heightmap.height + (j+1)) * channels + c] * (COLOR_SEP * (channels-c))/10000;
                height[2] += heightmap.image_data[((i+1) * heightmap.height + j) * channels + c] * (COLOR_SEP * (channels-c))/10000;
                height[3] += heightmap.image_data[((i+1) * heightmap.height + (j+1)) * channels + c] * (COLOR_SEP * (channels-c))/10000;
            }
/*            glTexCoord2f((float)i/(float)heightmap.width,(float)j/(float)heightmap.height);
            glVertex3f(i,height[0], j);
            glTexCoord2f(i/(float)heightmap.width,(float)(j+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(i+1)/(float)heightmap.width,(float)j/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);

            glTexCoord2f((float)i/(float)heightmap.width,(float)(j+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(i+1)/(float)heightmap.width,(float)(j+1)/(float)heightmap.width);
            glVertex3f(i+1,height[3], j+1);
            glTexCoord2f((float)(i+1)/(float)heightmap.width,(float)j/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);*/
            glTexCoord2f((float)j/(float)heightmap.height,(float)i/(float)heightmap.width);
            glVertex3f(i,height[0], j);
            glTexCoord2f(j/(float)heightmap.height,(float)(i+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(j+1)/(float)heightmap.height,(float)i/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);

            glTexCoord2f((float)j/(float)heightmap.height,(float)(i+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(j+1)/(float)heightmap.height,(float)(i+1)/(float)heightmap.width);
            glVertex3f(i+1,height[3], j+1);
            glTexCoord2f((float)(j+1)/(float)heightmap.height,(float)i/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);
        }
    }
    glEnd();
}

float get_terrain_height(Texture heightmap, float x, float z)
{
    if (x < 0 || z < 0 || x > heightmap.width || z > heightmap.height)
        return 0;
    int channels = heightmap.color_bits/8;
    int i = (int)x;
    int j = (int)z;
    float height = 0;
    for (int c = 0 ; c < channels ; c++)
        height += heightmap.image_data[(i * heightmap.height + j) * channels + c] * (COLOR_SEP * (channels-c))/10000;
    return height;
}

void manage_inputs()
{
    if (InputStatus::is_key_pressed(0x1B))
    {
        glutDestroyWindow(glutGetWindow());
        exit(EXIT_SUCCESS);
    }
    else
    {
        glClearColor(0.1,0.1,0.5,1);
    }

    if (InputStatus::is_special_key_pressed(GLUT_KEY_RIGHT))
    {
        camera.translate({0,0,0.1});
    }
    if (InputStatus::is_special_key_pressed(GLUT_KEY_LEFT))
    {
        camera.translate({0,0,-0.1});
    }
    if (InputStatus::is_key_pressed(' '))
    {
        camera.translate({0,0.1,0});
    }
    if (InputStatus::is_special_key_pressed(GLUT_KEY_PAGE_DOWN))
    {
        camera.translate({0,-0.1,0});
    }

    if (InputStatus::is_special_key_pressed(GLUT_KEY_UP))
    {
        camera.translate({0.1,0,0});
    }

    if (InputStatus::is_special_key_pressed(GLUT_KEY_DOWN))
    {
        camera.translate({-0.1,0,0});
    }

    camera.translate({0,get_terrain_height(terrain_heightmap, camera.get_position().x, camera.get_position().z)-camera.get_position().y+3, 0});

    if (InputStatus::is_key_pressed('p'))
        time_flow = time_flow == false;

    // Get mouse delta since last frame
    static Vec2i mouse_delta;
    mouse_delta = InputStatus::get_mouse_delta(true);
    if (mouse_delta.x != 0 || mouse_delta.y != 0)
    {
        camera.rotate({0,
                       -mouse_delta.x / InputStatus::mouse_sensitivity.x,
                       -mouse_delta.y / InputStatus::mouse_sensitivity.y});
/*        camera.rotate({0,
                       -mouse_delta.x / InputStatus::mouse_sensitivity.x,
                       0});*/
        raptor_angle += -mouse_delta.x*360 / (InputStatus::mouse_sensitivity.x*PI);
    }
}

void display()
{
    manage_inputs();

    float angleY = 1*timer_ticks;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    // Prepare view
    glLoadIdentity();
    gluPerspective(60,aspect_ratio,0.1,30000);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    camera.look();

    //display_rotating_pyramid(-5,-2,-5,2,4,angleY);
    //display_rotating_pyramid(5,-2,0,1,5,angleY);
    //display_rotating_pyramid(-2,-2,4,3,2,angleY);

    glPushMatrix();
    display_tree(70,2,120,8,10,tree_texture);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0,0,0);
    glScalef(1,1,1);
    draw_heightmap(terrain_heightmap, terrain_texture);
    glPopMatrix();


    glPushMatrix();
    glTranslatef(camera.get_position().x, camera.get_position().y-2, camera.get_position().z);
    glRotatef(raptor_angle,0,1,0);
    raptor.draw_model();
    glPopMatrix();

    if ((camera.get_position() - bunny_pos).magnitude() < 3)
    {
        bunny_pos.x = camera.get_position().x+rand()%20-10;
        bunny_pos.z = camera.get_position().z+rand()%20-10;
        bunny_pos.y = get_terrain_height(terrain_heightmap,bunny_pos.x, bunny_pos.z) + 2;
        bunny_angle = angleY;
        timer = 0;
    }

    glPushMatrix();
    glTranslatef(bunny_pos.x, bunny_pos.y, bunny_pos.z);
    glRotatef(bunny_angle, 0, 1, 0);
    bunny.draw_model();
    glPopMatrix();

    if (timer > LIFETIME)
    {
        glutDestroyWindow(glutGetWindow());
        exit(0);
    }

    glutSwapBuffers();
}

void reshape(int new_x, int new_y)
{
    glViewport(0,0,new_x,new_y);

    aspect_ratio = (double)new_x/new_y;
    InputStatus::window_size.x = new_x;
    InputStatus::window_size.y = new_y;
}

void update_angle(int value)
{
    if (time_flow)
        timer ++;
    timer_ticks++;
    glutTimerFunc(50,update_angle,0);
}

int main(int argc, char** argv)
{
    glutInit(&argc,argv);
    //GLFWwindow* window;

	/* Initialize the library */
	if ( !glfwInit() )
	{
		return -1;
	}
	printf("%s\n", "glfwInit success");

    #ifdef __APPLE__
        /* We need to explicitly ask for a 3.2 context on OS X */
        glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow( 1280, 720, "Frogland Saga", NULL, NULL );
    if (!window)
    {
        glfwTerminate();
        return -1;
    }
    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // Generate window with GLUT
    glutInitDisplayMode(GLUT_RGB| GLUT_DEPTH | GLUT_DOUBLE);
    //glutCreateWindow("Eat or die");
    glutIgnoreKeyRepeat(true);
    glutSetCursor(GLUT_CURSOR_NONE);

    // Init OpenGL
    glClearColor(10,10,15,1);
    glClearDepth(1.0);
    glEnable(GL_DEPTH_TEST);

    // Setup OPenGL to use textures
    glEnable(GL_TEXTURE_2D);
    glAlphaFunc(GL_GREATER, 0.5);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT/GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT/GL_CLAMP);
    glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Load and generate tree texture
    tree_texture.load_rgba_tga("../resources/arbre.tga","../resources/arbre_masque.tga");
    glGenTextures(1,tree_texture.opengl_id);
    // TODO : Put in the Texture class
    glBindTexture(GL_TEXTURE_2D,tree_texture.opengl_id[0]);
    gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA8,tree_texture.width,tree_texture.height,GL_RGBA,GL_UNSIGNED_BYTE,tree_texture.image_data);

    // Load terrain heightmap
    //terrain_heightmap.load_rgba_tga("../resources/arbre.tga","../resources/arbre_masque.tga");
    terrain_heightmap.load_rgb_tga("../resources/heightmap2.tga");
    terrain_texture.load_rgb_tga("../resources/texture_terrain.tga");

    // Load and generate raptor texture
    raptor_texture.load_rgb_tga("../resources/RAPTOR.tga");
    glGenTextures(1, raptor_texture.opengl_id);
    // TODO : Put in the Texture class
    glBindTexture(GL_TEXTURE_2D, raptor_texture.opengl_id[0]);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, raptor_texture.width, raptor_texture.height, GL_RGB, GL_UNSIGNED_BYTE, raptor_texture.image_data);

    bunny_texture.load_rgb_tga("../resources/mouse_texture.tga");
    glGenTextures(1, bunny_texture.opengl_id);
    glBindTexture(GL_TEXTURE_2D, bunny_texture.opengl_id[0]);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, bunny_texture.width, bunny_texture.height, GL_RGB, GL_UNSIGNED_BYTE, bunny_texture.image_data);


    raptor.load_ascii_off_file("../resources/RAPTOR.off");
    raptor.assign_texture(raptor_texture);
    raptor.set_scaling(0.01,0.01,0.01);
    raptor.set_rotation(-90,1,0,0);
    raptor_angle = -135;

    bunny.load_obj_file("../resources/mouse.obj");
    bunny.assign_texture(bunny_texture);
    bunny.set_scaling(1,1,1);
    bunny_pos = {100,3,100};

    glViewport(0,0,glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));

	glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutTimerFunc(50,update_angle,0);
    InputStatus::register_glut_callbacks();

    // Enters main loop, managed by GLUT
    glutMainLoop();

    glfwTerminate();

	return 0;
}
