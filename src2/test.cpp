/* Ask for an OpenGL Core Context */

#include <GL/glew.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
 
//#include "GLFW/linmath.h"
 
#include <stdlib.h>
#include <stdio.h>

#include <GL/glut.h>

#define GLFW_INCLUDE_GLCOREARB

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

#include "displayers.h"
#include "DataHandling/Texture.h"
#include "DataHandling/Model3D.h"
#include "InputStatus.h"
#include "Camera.h"

#define COLOR_SEP 50

//#include <GLFW/glfw3.h>
#include <iostream>

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}



/****************************************************************************\
 *                                                                            *
 *                            Variables GLOBALES                              *
 *                                                                            *
 \****************************************************************************/
GLFWwindow    *win = NULL;
//INPUT2     *inp = NULL;
//TIMER     *tim = NULL;
volatile unsigned long long int timer_ticks = 0;

float incrementAngleY = 0;
int timer = 0;
//////////////////// TP2 ////////////////////
static Texture  texture_arbre;	// � utiliser dans l'exercice 1.1 TP2
static Texture  texture_log;	// � utiliser dans l'exercice 2.1 TP2
static Texture  texture_raptor; // � utiliser dans l'exercice 3.1 TP2
static Texture texture_frog;
static Texture score_text;

static Model3D  geometrie_raptor; // � utiliser dans l'exercice 3.1 TP2
static Model3D  geometrie_frog; // � utiliser dans l'exercice 3.1 TP2

float *positions_arbres_X = NULL; // a utiliser pour cr�er la foret
float *positions_arbres_Z = NULL; // a utiliser pour cr�er la foret
int SIZE_LAND = 1100;

static Texture heightmap; // � utiliser pour generer le geometrie du terrain 3D (exercice 1.1 TP3) 
static Texture texture_terrain; // � utiliser pour texturer le terrain 3D (exercice 1.1 TP3) 
float horisontal_resolution = 10; // � utiliser pour g�n�rer le terrain
float vertical_scale = 0.3; // � utiliser pour g�n�rer le terrain

//Camera *cam = NULL;
static Camera cam({0,0,0},{0,0,0},{0,0,0});
float posX = 0;
float posY = 30;
float posZ = 0;
float angleX = 0;
float angleY = 0;
float angleZ = 0;

float frogX = 0;
float frogY = 0;
float frogZ = 0;
float frogAngleX = 0;
float frogAngleZ = 0;

struct raptor { float x; float y; float z; int angleX; int angleY; int angleZ; float moveX; float moveZ; int counter; };

raptor raptor1 = { 0,30,0,0,0,0,0,0 };
raptor raptor2 = { 0,30,0,0,0,0,0,0 };
raptor raptor3 = { 0,30,0,0,0,0,0,0 };

/*
float raptorPosX = 0;
float raptorPosY = 30;
float raptorPosZ = 0;
float raptorAngleX = 0;
float raptorAngleY = 0;
float raptorAngleZ = 0;
*/
float xInfluence = 0;
float zInfluence = 0;

float playerAngleX = 90;
float playerAngleY = 90;

//int raptorCounter = 0;
int raptorSpeed = 38;		

/*
float raptorXInfluence;
float raptorYInfluence;
*/

float appleRotation = 0;

const int APPLE_COUNT = 1;

float depthTolerance = 75.0f;

float playerSpeed = 24.0f;

int score = 0;

float FROG_SIZE = 2.3f;

float appleSize = 3.0f;

float raptorSize = 1.5f;
/****************************************************************************\
*                                                                            *
*                             Variables LOCALES                              *
*                                                                            *
\****************************************************************************/

struct coord { float x; float y; float z; };

bool inactiveApple[APPLE_COUNT];















void initApples()
{
	float I, J, pp, Y_arbre, testX, testY;
	int i, j;
	int give_up = 100;
	for (int ii = 0; ii < APPLE_COUNT; ii++) {
		do
		{
			testX = SIZE_LAND * (2 * rand() - RAND_MAX) / RAND_MAX;
			testY = SIZE_LAND * (2 * rand() - RAND_MAX) / RAND_MAX;

			I = testX / horisontal_resolution;
			J = testY / horisontal_resolution;
			i = (int)I + (int)heightmap.height / 2;
			j = (int)J + (int)heightmap.height / 2;

			float pp = ((float)heightmap.image_data[3 * (i + heightmap.width*(j + 0))] +
				(float)heightmap.image_data[3 * (i + heightmap.width*(j + 1))] +
				(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 0))] +
				(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 1))]) / 4;

			Y_arbre = vertical_scale * pp;
			give_up--;
		} while (Y_arbre < depthTolerance-40 && give_up > 0);

		positions_arbres_X[ii] = testX;
		positions_arbres_Z[ii] = testY;
		inactiveApple[ii] = false;
	}


}


/********************************************************************\
*                                                                    *
*  D�marre l'application (avant la main_loop). Renvoie false si      *
*  il y a une erreur.                                                *
*                                                                    *
\********************************************************************/

bool timid_start()
{
	
	// Can't find this function! create_context(window);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);					// set clear color for color buffer (RGBA, black)
	//glViewport(0, 0, win->Xres, win->Yres);				// zone de rendu (tout l'�cran)
	//glViewport(0,0,glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));

	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LESS);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// mapping quality = best
	glFrontFace(GL_CCW);								// front of face is defined counter clock wise
	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);					// back of a face is made of lines
	glCullFace(GL_BACK);								// cull back face only
	glDisable(GL_CULL_FACE);	

	texture_arbre.load_rgba_tga("images/texture_arbre.tga", "images/arbre_masque.tga");
	glGenTextures(1, texture_arbre.opengl_id);				// cr�e un "nom" de texture (un identifiant associ� � la texture)
	glBindTexture(GL_TEXTURE_2D, texture_arbre.opengl_id[0]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA8,texture_arbre.width,texture_arbre.height,GL_RGBA,GL_UNSIGNED_BYTE,texture_arbre.image_data);

	//texture_frog = new Texture();
	texture_frog.load_rgb_tga("images/tex_frog.tga");
  	glGenTextures(1, texture_frog.opengl_id);
  	glBindTexture(GL_TEXTURE_2D, texture_frog.opengl_id[0]);
  	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_frog.width, texture_frog.height, GL_RGB, GL_UNSIGNED_BYTE, texture_frog.image_data);
	geometrie_frog.load_ascii_off_file("images/frog_cube.off");
  	geometrie_frog.assign_texture(texture_frog);
	geometrie_frog.set_scaling(1,1,1);


	//texture_raptor = new Texture();
	texture_raptor.load_rgb_tga("images/texture_raptor.tga");
	glGenTextures(1, texture_raptor.opengl_id);				// cr�e un "nom" de texture (un identifiant associ� � la texture)
  	glBindTexture(GL_TEXTURE_2D, texture_raptor.opengl_id[0]);
  	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_frog.width, texture_raptor.height, GL_RGB, GL_UNSIGNED_BYTE, texture_raptor.image_data);
	//texture_logo = new Texture();
	geometrie_raptor.load_ascii_off_file("images/RAPTORCopy.off");
  	geometrie_raptor.assign_texture(texture_raptor);
	geometrie_raptor.set_scaling(1,1,1);
	positions_arbres_X = new float[APPLE_COUNT];
	positions_arbres_Z = new float[APPLE_COUNT];


	/////////////////// TP3 Exercice 1.1 //////////////////	
	//heightmap = new Texture();
	heightmap.load_rgb_tga("images/heightmap.tga");
	//texture_terrain = new Texture();
	texture_terrain.load_rgb_tga("images/texture_terrain.tga");
	//glGenTextures(1, texture_terrain.opengl_id);				// cr�e un "nom" de texture (un identifiant associ� � la texture)


	initApples();
	//InputStatus::register_glut_callbacks();

	return true;


}

bool start()
{
	
	//srand(time(NULL));
	//// initialisations de base
	//// -----------------------
	////win = new WINDOW();									// pr�pare la fen�tre
	////win->create(800, 600, 16, 60, false);			// cr�e la fen�tre
//
//
    ///* Initialize the library */
    //if (!glfwInit())
    //    return -1;
//
    //#ifdef __APPLE__
    //  /* We need to explicitly ask for a 3.2 context on OS X */
    //  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    //  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
    //  glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    //  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //#endif
//
    ///* Create a windowed mode window and its OpenGL context */
    //win = glfwCreateWindow(800, 600, "Frogland Saga", NULL, NULL);
    //if (!win)
    //{
    //    glfwTerminate();
    //    return -1;
    //}
//
	///* Make the window's context current */
	//glfwMakeContextCurrent(win);


	//tim = new TIMER();									// cr�e un timer
	//cam = new Camera();

	//inp = new INPUT2(win);								// initialise la gestion clavier souris
	//create_context(*win);								// cr�e le contexte OpenGL sur la fen�tre
	//init_font(*win, "Courier");							// initialise la gestion de texte avec OpenGL


	// initialisations d'OpenGL
	// ------------------------
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);					// set clear color for color buffer (RGBA, black)
	//glViewport(0, 0, win->Xres, win->Yres);				// zone de rendu (tout l'�cran)
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LESS);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// mapping quality = best
	glFrontFace(GL_CCW);								// front of face is defined counter clock wise
	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);					// back of a face is made of lines
	glCullFace(GL_BACK);								// cull back face only
	glDisable(GL_CULL_FACE);						    // disable back face culling


	//win->set_title("Frogland Saga");

	//texture_arbre = new Texture();

	//texture_arbre->load_texture("texture_arbre.tga", NULL);

	texture_arbre.load_rgba_tga("images/texture_arbre.tga", "images/arbre_masque.tga");

	glGenTextures(1, texture_arbre.opengl_id);				// cr�e un "nom" de texture (un identifiant associ� � la texture)
	glBindTexture(GL_TEXTURE_2D, texture_arbre.opengl_id[0]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA8,texture_arbre.width,texture_arbre.height,GL_RGBA,GL_UNSIGNED_BYTE,texture_arbre.image_data);

	//score_text = new Texture();
	score_text.load_rgba_tga("images/score_text.tga", "images/score_masque.tga");
	
	//texture_frog = new Texture();
	texture_frog.load_rgb_tga("images/tex_frog.tga");
  	glGenTextures(1, texture_frog.opengl_id);
  	glBindTexture(GL_TEXTURE_2D, texture_frog.opengl_id[0]);
  	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_frog.width, texture_frog.height, GL_RGB, GL_UNSIGNED_BYTE, texture_frog.image_data);


	//texture_raptor = new Texture();
	texture_raptor.load_rgb_tga("images/texture_raptor.tga");
	glGenTextures(1, texture_raptor.opengl_id);				// cr�e un "nom" de texture (un identifiant associ� � la texture)
  	glBindTexture(GL_TEXTURE_2D, texture_raptor.opengl_id[0]);
  	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_frog.width, texture_raptor.height, GL_RGB, GL_UNSIGNED_BYTE, texture_raptor.image_data);
  	geometrie_raptor.assign_texture(texture_raptor);
	//texture_logo = new Texture();


	/////////////////// TP2 Exercice 3.1 //////////////////	
	//geometrie_frog = new Model3D();
	geometrie_frog.load_ascii_off_file("images/frog_cube.off");
  	geometrie_frog.assign_texture(texture_frog);
	//geometrie_raptor = new Model3D();
	geometrie_raptor.load_ascii_off_file("images/RAPTORCopy.off");
  	geometrie_raptor.assign_texture(texture_raptor);

	positions_arbres_X = new float[APPLE_COUNT];
	positions_arbres_Z = new float[APPLE_COUNT];


	/////////////////// TP3 Exercice 1.1 //////////////////	
	//heightmap = new Texture();
	heightmap.load_rgb_tga("images/heightmap.tga");
	//texture_terrain = new Texture();
	texture_terrain.load_rgb_tga("images/texture_terrain.tga");
	//glGenTextures(1, texture_terrain.opengl_id);				// cr�e un "nom" de texture (un identifiant associ� � la texture)


	initApples();


	return true;
}


void display_arbre()
{

	// TODO technically dont deen this? glBindTexture(GL_TEXTURE_2D, texture_arbre.opengl_id[0]);	// et on active ce "nom" comme texture courante (d�finie plus bas)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	
	// TODO technically don't need this? gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, /*texture_arbre->img_color->lenx*/texture_arbre.width, /*texture_arbre->img_color->leny*/texture_arbre.height, GL_RGBA, GL_UNSIGNED_BYTE, texture_arbre.image_data);
	

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.4); // on utilise cette fonction pour controller la transparence dans l'exercice 1.2 de TP 2

	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_FILL);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex3f(-5, 0, 0);
	glTexCoord2f(0, 1); glVertex3f(-5, 20, 0);
	glTexCoord2f(1, 1); glVertex3f(5, 20, 0);
	glTexCoord2f(1, 0); glVertex3f(5, 0, 0);
	glEnd();

	glPushMatrix();
	glRotatef(90, 0, 1, 0);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex3f(-5, 0, 0);
	glTexCoord2f(0, 1); glVertex3f(-5, 20, 0);
	glTexCoord2f(1, 1); glVertex3f(5, 20, 0);
	glTexCoord2f(1, 0); glVertex3f(5, 0, 0);
	glEnd();
	glPopMatrix();

	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
}


/*
* Is the frog near an apple?
*/
bool isNear(coord appleCoord, coord playerCoord)
{
	int prox = 90;

	if (appleCoord.x > (playerCoord.x - prox) && appleCoord.x < (playerCoord.x + prox) && appleCoord.z > (playerCoord.z - prox) && appleCoord.z < (playerCoord.z + prox))
	{
		return true;
	}
	return false;
}

/*
* Draw how many apples have been eaten
*/
void drawUIScore(float posX, float posZ)
{
	glBindTexture(GL_TEXTURE_2D, score_text.opengl_id[0]);	// et on active ce "nom" comme texture courante (d�finie plus bas)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap


	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, score_text.width, score_text.height, GL_RGBA, GL_UNSIGNED_BYTE, score_text.image_data);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	//glAlphaFunc(GL_GREATER, 0.4); // on utilise cette fonction pour controller la transparence dans l'exercice 1.2 de TP 2

	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_FILL);
	glPushMatrix();
	glTranslatef(posX, 540, posZ);
	/*
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex3f(-10 * appleSize, 0, 0);
	glTexCoord2f(0, 1); glVertex3f(-10 * appleSize, 20 * appleSize, 0);
	glTexCoord2f(1, 1); glVertex3f(10 * appleSize, 20 * appleSize, 0);
	glTexCoord2f(1, 0); glVertex3f(10 * appleSize, 0, 0);
	glEnd();
	*/
	//glPushMatrix();



	glBegin(GL_QUADS);

	if (score >= 10)
	{
		glTexCoord2f(0.5, 0); glVertex3f(0, 0, 0);
		glTexCoord2f(0.5, 1); glVertex3f(0, 70, 0);
		glTexCoord2f(1, 1); glVertex3f(300, 70, 0);
		glTexCoord2f(1, 0); glVertex3f(300, 0, 0);
	}
	else if(score > 0)
	{
		glTexCoord2f((5.4*score)*0.01 - 0.054, 0); glVertex3f(0, 0, 0);
		glTexCoord2f((5.4*score)*0.01 - 0.054, 1); glVertex3f(0, 70, 0);
		glTexCoord2f((5.4*score)*0.01, 1); glVertex3f(70, 70, 0);
		glTexCoord2f((5.4*score)*0.01, 0); glVertex3f(70, 0, 0);
	}
	glEnd();
	//glPopMatrix();

	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


/*
* Draw the apples in the UI
*/
void drawUIApple(float posX, float posZ)
{
	glBindTexture(GL_TEXTURE_2D, texture_arbre.opengl_id[0]);	// et on active ce "nom" comme texture courante (d�finie plus bas)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap


	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_arbre.width, texture_arbre.height, GL_RGBA, GL_UNSIGNED_BYTE, texture_arbre.image_data);
	
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.4); // on utilise cette fonction pour controller la transparence dans l'exercice 1.2 de TP 2

	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_FILL);
	glPushMatrix();
	glTranslatef(posX, 540, posZ);
	glRotatef(appleRotation, 0, 1, 0);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex3f(-10 * appleSize, 0, 0);
	glTexCoord2f(0, 1); glVertex3f(-10 * appleSize, 20 * appleSize, 0);
	glTexCoord2f(1, 1); glVertex3f(10 * appleSize, 20 * appleSize, 0);
	glTexCoord2f(1, 0); glVertex3f(10 * appleSize, 0, 0);
	glEnd();
	glRotatef(-appleRotation, 0, 1, 0);

	glPushMatrix();
	glRotatef(appleRotation + 90, 0, 1, 0);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex3f(-10 * appleSize, 0, 0);
	glTexCoord2f(0, 1); glVertex3f(-10 * appleSize, 20 * appleSize, 0);
	glTexCoord2f(1, 1); glVertex3f(10 * appleSize, 20 * appleSize, 0);
	glTexCoord2f(1, 0); glVertex3f(10 * appleSize, 0, 0);
	glEnd();
	glPopMatrix();
	
	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


void drawApple(int ii, coord playerCoord)
{
	glPushMatrix();
	//glTranslatef(positions_arbres_X[i], O, positions_arbres_Z[i]);

	// quand le terrain n'est pas plat, on doit positioner les arbres sur le terrain ... donc la position sur Y depend de l'hauteur du terrain
	float I = positions_arbres_X[ii] / horisontal_resolution;
	float J = positions_arbres_Z[ii] / horisontal_resolution;
	int i = (int)I + (int)heightmap.width / 2;
	int j = (int)J + (int)heightmap.height / 2;

	float pp = ((float)heightmap.image_data[3 * (i + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + heightmap.width*(j + 1))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 1))]) / 4;



	float Y_arbre = vertical_scale * pp;
	// end calcul positionement sur Y

	glTranslatef(positions_arbres_X[ii], Y_arbre, positions_arbres_Z[ii]);
	coord appleCoord = { positions_arbres_X[ii], Y_arbre, positions_arbres_Z[ii] };
	if (!inactiveApple[ii] && isNear(appleCoord, playerCoord))
	{
		inactiveApple[ii] = true;
		score++;
	}
	if (!inactiveApple[ii])
	{
		glRotatef(appleRotation, 0, 1, 0);

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(-10 * appleSize, 0, 0);
		glTexCoord2f(0, 1); glVertex3f(-10 * appleSize, 20 * appleSize, 0);
		glTexCoord2f(1, 1); glVertex3f(10 * appleSize, 20 * appleSize, 0);
		glTexCoord2f(1, 0); glVertex3f(10 * appleSize, 0, 0);
		glEnd();
		glRotatef(-appleRotation, 0, 1, 0);

		glPushMatrix();
		glRotatef(appleRotation+90, 0, 1, 0);
		//display_apple(positions_arbres_X[ii], Y_arbre, positions_arbres_Z[ii],appleRotation, 90.0f);

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(-10 * appleSize, 0, 0);
		glTexCoord2f(0, 1); glVertex3f(-10 * appleSize, 20 * appleSize, 0);
		glTexCoord2f(1, 1); glVertex3f(10 * appleSize, 20 * appleSize, 0);
		glTexCoord2f(1, 0); glVertex3f(10 * appleSize, 0, 0);
		glEnd();
		glPopMatrix();
	}
	glPopMatrix();
}


void simple_display_frog(float x, float y, float z, float angleX, float angleY)
{
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();
	glPushMatrix();
	//glTranslatef(rand()%100,rand()%100,rand()%100);
	glTranslatef(x,y,z);
	//geometrie_frog.set_scaling(rand()%100,rand()%100,rand()%100);
	glRotatef(180,0,0,1);
	geometrie_frog.draw_model();
	glPopMatrix();
}


void display_frog(float x, float y, float z, float angleX, float angleY)
{
	
	//glBindTexture(GL_TEXTURE_2D, texture_frog->OpenGL_ID[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante
	
	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap
	
	//if (texture_frog->isRGBA)
	//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_frog->img_color->lenx, texture_frog->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_frog->img_all);
	//else
	//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_frog->img_color->lenx, texture_frog->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_frog->img_color->data);

	glEnable(GL_TEXTURE_2D);
	
	glPushMatrix();

	glTranslatef(x, y, z); // pour le positioner sur le terrain
	glRotatef(180, 0, 0, 1);
	glRotatef(180, 1, 0, 0);
	glRotatef(angleY-180, 0, 1, 0);
	glScalef(FROG_SIZE, FROG_SIZE, FROG_SIZE);


	//glBegin(GL_TRIANGLES);
	//for (int i = 0; i < geometrie_frog->nb_triangles; i++) {
	//	glTexCoord2f(geometrie_frog->points[geometrie_frog->faces[i].a].ucol, geometrie_frog->points[geometrie_frog->faces[i].a].vcol);		
	//	//glTexCoord2f(1.0, 1.0);
	//	glVertex3f(geometrie_frog->points[geometrie_frog->faces[i].a].x, geometrie_frog->points[geometrie_frog->faces[i].a].y, geometrie_frog->points[geometrie_frog->faces[i].a].z);
	//	glTexCoord2f(geometrie_frog->points[geometrie_frog->faces[i].b].ucol, geometrie_frog->points[geometrie_frog->faces[i].b].vcol);
	//	//glTexCoord2f(0.0, 1.0);
	//	glVertex3f(geometrie_frog->points[geometrie_frog->faces[i].b].x, geometrie_frog->points[geometrie_frog->faces[i].b].y, geometrie_frog->points[geometrie_frog->faces[i].b].z);
	//	//glTexCoord2f(0.0, 0.0);
	//	glTexCoord2f(geometrie_frog->points[geometrie_frog->faces[i].c].ucol, geometrie_frog->points[geometrie_frog->faces[i].c].vcol);
	//	glVertex3f(geometrie_frog->points[geometrie_frog->faces[i].c].x, geometrie_frog->points[geometrie_frog->faces[i].c].y, geometrie_frog->points[geometrie_frog->faces[i].c].z);
	//}

	//glEnd();
  	geometrie_frog.draw_model();
	glPopMatrix();
	/*glDisable(GL_TEXTURE_2D);*/
}



///////////////////  TP2 bonus //////////////////
void display_optimized_forest(coord playerCoord)
{

	//glBindTexture(GL_TEXTURE_2D, texture_arbre.opengl_id[0]);	// et on active ce "nom" comme texture courante (d�finie plus bas)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	//if (texture_arbre->isRGBA)
	//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_arbre->img_color->lenx, texture_arbre->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_arbre->img_all);
	//else
	//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_arbre->img_color->lenx, texture_arbre->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_arbre->img_color->data);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.4); // on utilise cette fonction pour controller la transparence dans l'exercice 1.2 de TP 2

	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_FILL);

	for (int ii = 0; ii < APPLE_COUNT; ii++) {
		drawApple(ii, playerCoord);
	}

	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
}
/////////////////// TP2 end bonus //////////////////	


void display_raptor(float x, float y, float z, float angleX, float angleY)
{
	//glBindTexture(GL_TEXTURE_2D, texture_raptor->OpenGL_ID[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	//if (texture_raptor->isRGBA)
	//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_raptor->img_all);
	//else
	//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_raptor->img_color->data);

	glEnable(GL_TEXTURE_2D);

	glPushMatrix();

	glTranslatef(x, y, z); // pour le positioner sur le terrain
	glRotatef(-90, 1, 0, 0);
	glRotatef(angleY, 0, 0, 1);
	glScalef(raptorSize,raptorSize,raptorSize);


	//glBegin(GL_TRIANGLES);
	//for (int i = 0; i < geometrie_raptor->nb_triangles; i++) {
	//	glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].a].ucol, geometrie_raptor->points[geometrie_raptor->faces[i].a].vcol);
	//	glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].a].x, geometrie_raptor->points[geometrie_raptor->faces[i].a].y, geometrie_raptor->points[geometrie_raptor->faces[i].a].z);
//
	//	glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].b].ucol, geometrie_raptor->points[geometrie_raptor->faces[i].b].vcol);
	//	glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].b].x, geometrie_raptor->points[geometrie_raptor->faces[i].b].y, geometrie_raptor->points[geometrie_raptor->faces[i].b].z);
//
	//	glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].c].ucol, geometrie_raptor->points[geometrie_raptor->faces[i].c].vcol);
	//	glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].c].x, geometrie_raptor->points[geometrie_raptor->faces[i].c].y, geometrie_raptor->points[geometrie_raptor->faces[i].c].z);
	//}
	//glEnd();
  	geometrie_raptor.draw_model();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}


bool raptorOutOfBounds(float raptorX, float raptorZ)
{
	if (raptorX < -SIZE_LAND / 2 ) return true;
	if (raptorX > SIZE_LAND / 2 ) return true;
	if (raptorZ < -SIZE_LAND / 2) return true;
	if (raptorZ > SIZE_LAND / 2) return true;

	return false;
}


void display_terrain()
{
  uint8_t channels = heightmap.color_bits/8;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture_terrain.opengl_id[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	 // charge le tableau de la texture en mémoire vidéo et crée une texture mipmap
    if (texture_terrain.color_bits == 32)
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_terrain.width, texture_terrain.height, GL_RGBA, GL_UNSIGNED_BYTE, texture_terrain.image_data);
    else
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_terrain.width, texture_terrain.height, GL_RGB, GL_UNSIGNED_BYTE, texture_terrain.image_data);
	glBegin(GL_TRIANGLES);
	
  /*for (int i = 0; i < (int)heightmap->lenx - 1; i++)
	{
		for (int j = 0; j < (int)heightmap->leny - 1; j++)
		{
			float I = i - (int)heightmap->lenx / 2;
			float J = j - (int)heightmap->leny / 2;

			glTexCoord2f((float)i / (float)heightmap->lenx, (float)j / (float)heightmap->leny);
			glVertex3f((I + 0)*horisontal_resolution, vertical_scale*(float)heightmap->data[3 * (i + heightmap->lenx*(j + 0))], (J + 0)*horisontal_resolution);

			glTexCoord2f((float)i / (float)heightmap->lenx, (float)(j + 1) / (float)heightmap->leny);
			glVertex3f((I + 0)*horisontal_resolution, vertical_scale*(float)heightmap->data[3 * (i + heightmap->lenx*(j + 1))], (J + 1)*horisontal_resolution);

			glTexCoord2f((float)(i + 1) / (float)heightmap->lenx, (float)j / (float)heightmap->leny);
			glVertex3f((I + 1)*horisontal_resolution, vertical_scale*(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 0))], (J + 0)*horisontal_resolution);

			glTexCoord2f((float)i / (float)heightmap->lenx, (float)(j + 1) / (float)heightmap->leny);
			glVertex3f((I + 0)*horisontal_resolution, vertical_scale*(float)heightmap->data[3 * (i + heightmap->lenx*(j + 1))], (J + 1)*horisontal_resolution);

			glTexCoord2f((float)(i + 1) / (float)heightmap->lenx, (float)(j + 1) / (float)heightmap->leny);
			glVertex3f((I + 1)*horisontal_resolution, vertical_scale*(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 1))], (J + 1)*horisontal_resolution);

			glTexCoord2f((float)(i + 1) / (float)heightmap->lenx, (float)j / (float)heightmap->leny);
			glVertex3f((I + 1)*horisontal_resolution, vertical_scale*(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 0))], (J + 0)*horisontal_resolution);
		}
	}
	glEnd();*/
  for (int i = 0 ; i < heightmap.height - 1; i++)
    {
        for (int j = 0 ; j < heightmap.width - 1; j++)
        {
            float height[] = {0,0,0,0};
            for (int c = 0 ; c < channels ; c++) {
                height[0] += heightmap.image_data[(i * heightmap.height + j) * channels + c] * (COLOR_SEP * (channels-c))/10000;
                height[1] += heightmap.image_data[(i * heightmap.height + (j+1)) * channels + c] * (COLOR_SEP * (channels-c))/10000;
                height[2] += heightmap.image_data[((i+1) * heightmap.height + j) * channels + c] * (COLOR_SEP * (channels-c))/10000;
                height[3] += heightmap.image_data[((i+1) * heightmap.height + (j+1)) * channels + c] * (COLOR_SEP * (channels-c))/10000;
            }
		/*   glTexCoord2f((float)i/(float)heightmap.width,(float)j/(float)heightmap.height);
            glVertex3f(i,height[0], j);
            glTexCoord2f(i/(float)heightmap.width,(float)(j+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(i+1)/(float)heightmap.width,(float)j/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);

            glTexCoord2f((float)i/(float)heightmap.width,(float)(j+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(i+1)/(float)heightmap.width,(float)(j+1)/(float)heightmap.width);
            glVertex3f(i+1,height[3], j+1);
            glTexCoord2f((float)(i+1)/(float)heightmap.width,(float)j/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);*/
            glTexCoord2f((float)j/(float)heightmap.height,(float)i/(float)heightmap.width);
            glVertex3f(i,height[0], j);
            glTexCoord2f(j/(float)heightmap.height,(float)(i+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(j+1)/(float)heightmap.height,(float)i/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);

            glTexCoord2f((float)j/(float)heightmap.height,(float)(i+1)/(float)heightmap.width);
            glVertex3f(i,height[1], j+1);
            glTexCoord2f((float)(j+1)/(float)heightmap.height,(float)(i+1)/(float)heightmap.width);
            glVertex3f(i+1,height[3], j+1);
            glTexCoord2f((float)(j+1)/(float)heightmap.height,(float)i/(float)heightmap.width);
            glVertex3f(i+1,height[2], j);
        }
    }
    glEnd();
	glDisable(GL_TEXTURE_2D);

}


raptor updateRaptor(raptor R)
{

	if (R.counter <= 0)
	{
		R.counter = rand() % 50;
		int roll = rand() % 4;
		R.moveX = 0;
		R.moveZ = 0;

		if (roll == 3)
		{
			R.moveX = raptorSpeed;
			R.angleY= 90;
		}
		else if (roll == 2)
		{
			R.moveX = -1 * raptorSpeed;
			R.angleY = 270;
		}
		else if (roll == 1)
		{
			R.moveZ = 1 * raptorSpeed;
			R.angleY = 0;
		}
		else
		{
			R.moveZ = -1 * raptorSpeed;
			R.angleY = 180;
		}
	}
	R.counter = R.counter - 2;

	int I = R.x / horisontal_resolution;
	int J = R.z / horisontal_resolution;
	int i = (int)I + (int)heightmap.width / 2;
	int j = (int)J + (int)heightmap.height / 2;

	int pp = ((float)heightmap.image_data[3 * (i + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + heightmap.width*(j + 1))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 1))]) / 4;

	R.y = 10 + vertical_scale * pp;

	R.x += R.moveX;
	R.z += R.moveZ;

	if (R.x < -SIZE_LAND / 2) {
		R.moveX *= -1;
		R.angleY = (R.angleY+180) % 360;
	}
	if (R.x > SIZE_LAND / 2) {
		R.moveX *= -1;
		R.angleY = (R.angleY + 180) % 360;
	}
	if (R.z < -SIZE_LAND / 2) {
		R.moveZ *= -1;
		R.angleY = (R.angleY + 180) % 360;
	}
	if (R.z > SIZE_LAND / 2) {
		R.moveZ *= -1;
		R.angleY = (R.angleY + 180) % 360;
	}

	return R;
}

void timid_main_loop(GLFWwindow *win)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glClearColor(0.329f, 0.427f, 0.6823f, 1.0f);

	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();


	int width, height;
  	glfwGetWindowSize(win, &width, &height);
	gluPerspective(60, (double)width / (double)height, 0.1, 40); 

	float lastPosX = posX;
	float lastPosY = posY;
	float lastPosZ = posZ;

	appleRotation += 8;

	// TODO: why this here angleX -= 0.1*(float)inp->Yrelmouse;
	//angleY -= 0.1*(float)inp->Xrelmouse;
	if (angleY > 360) angleY -= 360; 
  	if (angleY < 0) angleY += 360;
	if (angleX > 60) angleX = 60;	
  	if (angleX < -60) angleX = -60;

	float I = posX / horisontal_resolution;
	float J = posZ / horisontal_resolution;
	int i = (int)I + (int)heightmap.width / 2;
	int j = (int)J + (int)heightmap.height / 2;

	/*float pp = ((float)heightmap.image_data[3 * (i + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + heightmap.width*(j + 1))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 1))]) / 4;

	*/
	float pp = 0;
	posY = 60 + vertical_scale * pp; 


	coord playerPos = { posX, posY, posZ };

	raptor1 = updateRaptor(raptor1);
	raptor2 = updateRaptor(raptor2);
	raptor3 = updateRaptor(raptor3);
	coord enemyPos = { raptor1.x, raptor1.y, raptor1.z };


	if (posY < depthTolerance)
	{
		posX = lastPosX;
		posY = lastPosY;
		posZ = lastPosZ;
	}
	// GAME OVER
	if (isNear(enemyPos, playerPos) || isNear({ raptor2.x, raptor2.y, raptor2.z },playerPos) || isNear({ raptor2.x, raptor2.y, raptor2.z }, playerPos))
	{
		posX = 0;
		posY = 30;
		posZ = 0;
		angleX = 0;
		angleY = 0;
		angleZ = 0;
		score = 0;
		initApples();
	}

	cam.translate({posX + xInfluence,
						800, 
						posZ + zInfluence + 600});
  cam.rotate({-45, 0, angleZ});

  glMatrixMode(GL_MODELVIEW);
	// already did that right?	glLoadIdentity();
  cam.look();

  display_terrain();
	//////////// TP3: end exercice 1.1 le terrain
	//display_raptor(posX, posY, posZ, angleX, playerAngleY);

	display_frog(posX, posY, posZ, playerAngleX, playerAngleY);

		display_raptor(raptor1.x, raptor1.y, raptor1.z, raptor1.angleX, raptor1.angleY);
	/*else if (timer%3==1)	*/display_raptor(raptor2.x, raptor2.y, raptor2.z, raptor2.angleX, raptor2.angleY);

	drawUIApple(cam.get_position().x-450,cam.get_position().z-700);
	drawUIScore(cam.get_position().x - 400, cam.get_position().z - 700);


  //display_optimized_forest(playerPos);
}


/********************************************************************\
*                                                                    *
*  Boucle principale, appel�e pour construire une image, g�rer les   *
*  entr�es.                                                          *
*                                                                    *
\********************************************************************/
void main_loop()
{

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//				gestion des touches	et du temps													//
	//////////////////////////////////////////////////////////////////////////////////////////////////

	timer++;

	//inp->refresh();
	//tim->update_horloge();
	//inp->get_mouse_movement();
	

	//if (inp->keys[KEY_CODE_ESCAPE])
	//{
	//	PostMessage(win->handle, WM_CLOSE, 0, 0);	// Stoppe la "pompe � message" en y envoyant le message "QUIT"
	//}
  	if (InputStatus::is_key_pressed(0x1B))
    {
        glutDestroyWindow(glutGetWindow());
        exit(EXIT_SUCCESS);
    }
	
	/*
	if (tim->global_timer_25_Hz)				// augmente incrementAngleY tous les 20�me de seconde
		incrementAngleY += 1.0f;
	if (incrementAngleY >= 360) incrementAngleY -= 360;
	*/




	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	//////////////////////////////////////////////////////////////////////////////////////////////////
	//						�a commence ici															//
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	// votre code OpenGL ici


	// turquoise
	glClearColor(0.329f, 0.427f, 0.6823f, 1.0f);
	// end TP1 exercice 1.1


	glMatrixMode(GL_PROJECTION);  //la matrice de projection sera celle selectionnee
	//composition d'une matrice de projection
	glLoadIdentity(); //on choisit la matrice identit�
  int width, height;
  glfwGetWindowSize(win, &width, &height);
	gluPerspective(60, (double)width / (double)height, 1, 10000);   //mise en place d une proj angle de vue 60 deg near 10 far 30000

	// fonction � utiliser pour tous les exercices sauf le 2.1 du TP3: gluLookAt a une position fixe
	//	gluLookAt(0, 60, -180,		// position
	//		  0, 50, 1,		// point cible
	//		  0, 1, 0);		// vecteur up


//////////////// TP3 exercice 2.1 controle de la camera

	Vec3d dir = cam.get_gaze() - cam.get_position();
	float lastPosX = posX;
	float lastPosY = posY;
	float lastPosZ = posZ;


	if (InputStatus::is_special_key_pressed(GLUT_KEY_UP)) 
  	{
		posX += dir.x * playerSpeed;
		posZ += dir.z * playerSpeed;
		playerAngleY = 180;
	}
	if (InputStatus::is_special_key_pressed(GLUT_KEY_DOWN))
  	{
		posX -= dir.x * playerSpeed;
		posZ -= dir.z * playerSpeed;
		playerAngleY = 0;
	}


	if (InputStatus::is_special_key_pressed(GLUT_KEY_RIGHT))
  	{
		//posX += produit_vectoriel(dir, cam.orientation).x * playerSpeed;
		//posZ += produit_vectoriel(dir, cam.orientation).z * playerSpeed;
		posX += (dir.cross_product(cam.get_gaze())).x * playerSpeed;
		posZ += (dir.cross_product(cam.get_gaze())).z * playerSpeed;
		
    
    	playerAngleY = 90;
	}
	if (InputStatus::is_special_key_pressed(GLUT_KEY_LEFT))
	{
		//posX -= produit_vectoriel(dir, cam->orientation).x * playerSpeed;
			//posZ -= produit_vectoriel(dir, cam->orientation).z * playerSpeed;
			posX -= (dir.cross_product(cam.get_gaze())).x * playerSpeed;
		posZ -= (dir.cross_product(cam.get_gaze())).z * playerSpeed;
		playerAngleY = 270;
	}




	// Dev tool
	if (InputStatus::is_key_pressed('a'))
  	{
		xInfluence += 10;
		playerAngleX += 90;
		printf("xInfluence: %d", xInfluence);
		score++;
	}
	if (InputStatus::is_key_pressed('z'))
  	{
		xInfluence -= 10;
		printf("xInfluence: %d", xInfluence);
	}
	if (InputStatus::is_key_pressed('e')) 
 	{
		zInfluence += 10;
		printf("zInfluence: %d", zInfluence);
	}
	if (InputStatus::is_key_pressed('r'))
  	{
		zInfluence -= 10;
		printf("zInfluence: %d", zInfluence);
	}
	if (InputStatus::is_key_pressed('o'))
	{
		depthTolerance += 1;
	}
	if (InputStatus::is_key_pressed('p'))
	{
		depthTolerance -= 1;
	}

	appleRotation += 8;

	// TODO: why this here angleX -= 0.1*(float)inp->Yrelmouse;
	//angleY -= 0.1*(float)inp->Xrelmouse;
	if (angleY > 360) angleY -= 360; 
  	if (angleY < 0) angleY += 360;
	if (angleX > 60) angleX = 60;	
  	if (angleX < -60) angleX = -60;

	float I = posX / horisontal_resolution;
	float J = posZ / horisontal_resolution;
	int i = (int)I + (int)heightmap.width / 2;
	int j = (int)J + (int)heightmap.height / 2;

	float pp = ((float)heightmap.image_data[3 * (i + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + heightmap.width*(j + 1))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 0))] +
		(float)heightmap.image_data[3 * (i + 1 + heightmap.width*(j + 1))]) / 4;

	posY = 60 + vertical_scale * pp; 


	coord playerPos = { posX, posY, posZ };

	raptor1 = updateRaptor(raptor1);
	raptor2 = updateRaptor(raptor2);
	raptor3 = updateRaptor(raptor3);
	coord enemyPos = { raptor1.x, raptor1.y, raptor1.z };


	if (posY < depthTolerance)
	{
		posX = lastPosX;
		posY = lastPosY;
		posZ = lastPosZ;
	}
	// GAME OVER
	if (isNear(enemyPos, playerPos) || isNear({ raptor2.x, raptor2.y, raptor2.z },playerPos) || isNear({ raptor2.x, raptor2.y, raptor2.z }, playerPos))
	{
		posX = 0;
		posY = 30;
		posZ = 0;
		angleX = 0;
		angleY = 0;
		angleZ = 0;
		score = 0;
		initApples();
	}

	//TODO: should I use this? cam->update(point(posX,posY, posZ),angleX,angleY,angleZ);
	//cam->update(point(posX + xInfluence, 800, posZ + zInfluence + 600),
  // -45, 0, angleZ);
	cam.translate({posX + xInfluence,
						800, 
						posZ + zInfluence + 600});
	cam.rotate({-45, 0, angleZ});

	glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	cam.look();
	//gluLookAt(cam.get_position.x, cam.get_position.y, cam.get_position.z,		// position
	//	cam.get_gaze.x, cam.get_gaze.y, cam.get_gaze.z,		// point cible
	//	cam->orientation.x, cam->orientation.y, cam->orientation.z);		// vecteur up


	display_optimized_forest(playerPos);
	//////////// TP2: end bonus - optimized forest

	//////////// TP2: exercice 3.1 le raptor
	//////////// TP2: end exercice 3.1 le raptor

	//////////// TP3: exercice 1.1 le terrain
	display_terrain();
	//////////// TP3: end exercice 1.1 le terrain
	//display_raptor(posX, posY, posZ, angleX, playerAngleY);

	display_frog(posX, posY, posZ, playerAngleX, playerAngleY);
	//display_frog(0, 0, 0, angleX, playerAngleY);


	/*if (timer%3==0)		*/	display_raptor(raptor1.x, raptor1.y, raptor1.z, raptor1.angleX, raptor1.angleY);
	/*else if (timer%3==1)	*/display_raptor(raptor2.x, raptor2.y, raptor2.z, raptor2.angleX, raptor2.angleY);
	/*else					display_raptor(raptor3.x, raptor3.y, raptor3.z, raptor3.angleX, raptor3.angleY);*/

	//display_raptor(raptor1, raptor2);

	drawUIApple(cam.get_position().x-450,cam.get_position().z-700);
	drawUIScore(cam.get_position().x - 400, cam.get_position().z - 700);

	
	// TODO commented this out swap_buffer(win);	// affiche l'image compos�e � l'�cran
	glfwSwapBuffers(win);
}

int old_main(int argc, char** argv)
{
	if (start() == false)								// initialise la fen�tre et OpenGL
	{
		printf("%s\n", "start() failed !!!");
		//stop();
		return 1;
	}

	while (!glfwWindowShouldClose(win))
  	{
		  main_loop();
	}

	printf("%s\n", "terminating :)");
	glfwTerminate();
	return 0;
}

void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Vec3d dir = cam.get_gaze() - cam.get_position();

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
	
	if (key == GLFW_KEY_UP && action == GLFW_PRESS) 
  	{
		posX += dir.x * playerSpeed;
		posZ += dir.z * playerSpeed;
		playerAngleY = 180;

		frogX++;
		printf("frogX is now %f\n", frogX);
	}
	if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
  	{
		posX -= dir.x * playerSpeed;
		posZ -= dir.z * playerSpeed;
		playerAngleY = 0;

		frogX--;
		printf("frogX is now %f\n", frogX);
	}


	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
  	{
		//posX += produit_vectoriel(dir, cam.orientation).x * playerSpeed;
		//posZ += produit_vectoriel(dir, cam.orientation).z * playerSpeed;
		posX += (dir.cross_product(cam.get_gaze())).x * playerSpeed;
		posZ += (dir.cross_product(cam.get_gaze())).z * playerSpeed;
		
    
    	playerAngleY = 90;


		frogY++;
		printf("frogY is now %f\n", frogY);

	}
	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
	{
		//posX -= produit_vectoriel(dir, cam->orientation).x * playerSpeed;
			//posZ -= produit_vectoriel(dir, cam->orientation).z * playerSpeed;
		posX -= (dir.cross_product(cam.get_gaze())).x * playerSpeed;
		posZ -= (dir.cross_product(cam.get_gaze())).z * playerSpeed;
		playerAngleY = 270;
		printf("%s\n", "left!");

		frogY--;
		printf("frogY is now %f\n", frogY);
	}

	if (key == GLFW_KEY_W && action == GLFW_PRESS)
  	{
		frogZ++;
		printf("frogZ is now %f\n", frogZ);
	}
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
  	{
		frogZ--;
		printf("frogZ is now %f\n", frogZ);
	}
	

	if (key == GLFW_KEY_A && action == GLFW_PRESS)
  	{
		xInfluence += 10;
		playerAngleX += 90;
		printf("xInfluence: %d", xInfluence);
		score++;
	}
	if (key == GLFW_KEY_Z && action == GLFW_PRESS)
  	{
		xInfluence -= 10;
		printf("xInfluence: %d", xInfluence);
	}
	if (key == GLFW_KEY_E && action == GLFW_PRESS) 
 	{
		zInfluence += 10;
		printf("zInfluence: %d", zInfluence);
	}
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
  	{
		zInfluence -= 10;
		printf("zInfluence: %d", zInfluence);
	}
	if (key == GLFW_KEY_O && action == GLFW_PRESS)
	{
		depthTolerance += 1;
	}
	if (key == GLFW_KEY_P && action == GLFW_PRESS)
	{
		depthTolerance -= 1;
	}
}


int save_main(int argc, char** argv)
{
 	GLFWwindow* window;
	//glutInit(&argc,argv);
	/* Initialize the library */
	if ( !glfwInit() )
	{
		return -1;
	}
	printf("%s\n", "glfwInit success");

  #ifdef __APPLE__
    /* We need to explicitly ask for a 3.2 context on OS X */
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  #endif

	glfwSetErrorCallback(error_callback);

  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow( 1280, 720, "Frogland Saga", NULL, NULL );
  if (!window)
  {
     glfwTerminate();
     return -1;
  }
  printf("%s\n", "window creation success");
  
  /* Make the window's context current */
  glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, key_callback);
	int e = timid_start();
	if(e == false)
	{
		glfwTerminate();
		return -1;
	}
	printf("%s\n", "start function success?");

	int width, height;

  /* Loop until the user closes the window */
  while (!glfwWindowShouldClose(window))
  {

	glfwGetFramebufferSize(window, &width, &height);
 
    glViewport(0, 0, width, height);
    /* Render here */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the buffers

	


	//simple_display_frog(frogX, frogY, frogZ, frogAngleX, frogAngleZ);

	timid_main_loop(window);

    /* Swap front and back buffers */
    glfwSwapBuffers(window);

    /* Poll for and process events */
    glfwPollEvents();
  }

  glfwTerminate();
  return 0;
}


/************************************\
 * 
 * 		Samlple Project
 * 
 * **********************************/

int main(int argc, const char* argv[]) {
    if (!glfwInit()) {
        return 1;
    }

	srand(time(NULL));

    int monitorCount;

    GLFWmonitor** monitors = glfwGetMonitors(&monitorCount);

    GLFWmonitor* largestMonitor = monitors[0];

    const GLFWvidmode* largestVidmode = glfwGetVideoMode(largestMonitor);

    for (int i = 1; i < monitorCount; i += 1) {
        const GLFWvidmode* vidmode = glfwGetVideoMode(monitors[i]);

        if (vidmode->width * vidmode->height > largestVidmode->width * largestVidmode->height) {
            largestVidmode = vidmode;

            largestMonitor = monitors[i];
        }
    }

    #ifdef __APPLE__
		/* We need to explicitly ask for a 3.2 context on OS X */
		glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	#endif

		//glfwSetErrorCallback(error_callback);

	/* Create a windowed mode window and its OpenGL context */
	GLFWwindow* window = glfwCreateWindow( 1280, 720, "Frogland Saga", NULL, NULL );


    //GLFWwindow* window = glfwCreateWindow(largestVidmode->width, largestVidmode->height, "OpenGLTest", largestMonitor, nullptr);

    if (window == nullptr) {
        std::cerr << "Failed to create GLFW window." << std::endl;

        glfwTerminate();

        return 1;
    }

    glfwMakeContextCurrent(window);

	//glfwSetKeyCallback(window, key_callback);
	

    glewExperimental = GL_TRUE;

    glewInit();

    const GLubyte* openGLRenderer = glGetString(GL_RENDERER);

    const GLubyte* openGLVersion = glGetString(GL_VERSION);

    std::cout << "Renderer: " << openGLRenderer << std::endl;

    std::cout << "OpenGL version supported: " << openGLVersion << std::endl;

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LESS);

    float points[] = {
        0.0f, 0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f
    };

    GLuint triangleVBO = 0;

    glGenBuffers(1, &triangleVBO);

    glBindBuffer(GL_ARRAY_BUFFER, triangleVBO);

    glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW);

    GLuint triangleVAO = 0;

    glGenVertexArrays(1, &triangleVAO);

    glBindVertexArray(triangleVAO);

    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, triangleVBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    const char* vertexShaderSource =
    "#version 400\n"
    "in vec3 vp;"
    "void main() {"
    "  gl_Position = vec4 (vp, 1.0);"
    "}";

    const char* fragmentShaderSource =
    "#version 400\n"
    "out vec4 frag_colour;"
    "void main() {"
    "  frag_colour = vec4(1.0, 1.0, 1.0, 1.0);"
    "}";

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

    glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);

    glCompileShader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);

    glCompileShader(fragmentShader);

    GLuint shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, fragmentShader);

    glAttachShader(shaderProgram, vertexShader);

    glLinkProgram(shaderProgram);

    glfwSetKeyCallback(window, key_callback);

	int e = timid_start();
	if(e == false)
	{
		glfwTerminate();
		return -1;
	}
	printf("%s\n", "start function success?");

	int width, height;
  	glfwGetWindowSize(window, &width, &height);

    while (!glfwWindowShouldClose(window)) {
        //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shaderProgram);

		timid_main_loop(window);

		//simple_display_frog(frogX, frogY, frogZ, frogAngleX, frogAngleZ);
        
		glBindVertexArray(triangleVAO);

        glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(window);

        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
} 
