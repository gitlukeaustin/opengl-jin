//
// Created by trotfunky on 24/09/2019.
//

#ifndef TESTS_OPENGL_DISPLAYERS_H
#define TESTS_OPENGL_DISPLAYERS_H

#include <GLFW/glfw3.h>

#include "DataHandling/Texture.h"

/// Displays a square based pyramid from a base position, height and side length
/// \param x X coordinate of the center
/// \param y Y coordinate of the center
/// \param z Z coordinate of the center
/// \param c Side length of the square
/// \param h Height of the pyramid
void display_pyramid(float x, float y, float z, float c, float h);

/// Draw a pyramid rotated around its axis
void display_rotating_pyramid(float x, float y, float z, float c, float h, float alpha);

/// Draw a tree with two quadrilaterals
void display_tree(float x, float y, float z, float h, float w, const Texture& tree_texture);

#endif //TESTS_OPENGL_DISPLAYERS_H
