                                                                                                     /*
/##################################################################################################\
# Description : #                                                                                  #
#################                                                                                  #
#                                                                                                  #
#  Ce fichier est le point d'entr�e du moteur ( le "main()" ).                                     #
#                                                                                                  #
#                                                                                                  #
\##################################################################################################/
                                                                                                     */

#include "window.h"
#include "input.h"
#include "timer.h"
#include "ogl.h"
#include "load.h"
#include "camera.h"
#include "maths_3d.h"
#include "time.h"



/****************************************************************************\
*                                                                            *
*                            Variables GLOBALES                              *
*                                                                            *
\****************************************************************************/
WINDOW    *win = NULL;
INPUT2     *inp = NULL;
TIMER     *tim = NULL;
float incrementAngleY = 0;

//////////////////// TP2 ////////////////////
Texture  *texture_arbre = NULL;	// � utiliser dans l'exercice 1.1 TP2
Texture  *texture_logo = NULL;	// � utiliser dans l'exercice 2.1 TP2
Texture  *texture_raptor = NULL; // � utiliser dans l'exercice 3.1 TP2
Model3D  *geometrie_raptor = NULL; // � utiliser dans l'exercice 3.1 TP2
Model3D  *geometrie_frog = NULL; // � utiliser dans l'exercice 3.1 TP2

float *positions_arbres_X = NULL; // a utiliser pour cr�er la foret
float *positions_arbres_Z = NULL; // a utiliser pour cr�er la foret
int DIM_FOREST = 50;
int SIZE_LAND = 200;

//////////////////// TP3 ////////////////////
Image *heightmap = NULL; // � utiliser pour generer le geometrie du terrain 3D (exercice 1.1 TP3) 
Texture *texture_terrain=NULL; // � utiliser pour texturer le terrain 3D (exercice 1.1 TP3) 
float horisontal_resolution = 10; // � utiliser pour g�n�rer le terrain
float vertical_scale = 0.3; // � utiliser pour g�n�rer le terrain

// les variables suivantes sont � utiliser dans l'exercice 2.1 du TP3
CAMERA *cam = NULL;
float posX=0;
float posY=30;
float posZ=0;
float angleX=0;
float angleY=0;
float angleZ=0;

float raptorPosX = 0;
float raptorPosY = 30;
float raptorPosZ = 0;
float raptorAngleX = 0;
float raptorAngleY = 0;
float raptorAngleZ = 0;

float xInfluence = 0;
float zInfluence = 0;

float playerAngleX = 90;
float playerAngleY = 90;

int raptorCounter;
int raptorSpeed = 4;

float raptorXInfluence;
float raptorYInfluence;

float appleRotation = 0;

const int APPLE_COUNT = 50;

float playerSpeed = 7.0f;
/****************************************************************************\
*                                                                            *
*                             Variables LOCALES                              *
*                                                                            *
\****************************************************************************/

struct coord { float x; float y; float z; };

bool inactiveApple[APPLE_COUNT];



/********************************************************************\
*                                                                    *
*  D�marre l'application (avant la main_loop). Renvoie false si      *
*  il y a une erreur.                                                *
*                                                                    *
\********************************************************************/

bool start()
{
	srand(time(NULL));
	// initialisations de base
	// -----------------------
	win = new WINDOW();									// pr�pare la fen�tre
	win->create( 800, 600, 16 , 60 , false );			// cr�e la fen�tre

	tim = new TIMER();									// cr�e un timer
	cam= new CAMERA();

	inp = new INPUT2(win);								// initialise la gestion clavier souris
	create_context(*win);								// cr�e le contexte OpenGL sur la fen�tre
	init_font(*win, "Courier");							// initialise la gestion de texte avec OpenGL

  
	// initialisations d'OpenGL
	// ------------------------
	glClearColor(0.0f,0.0f,0.0f,1.0f);					// set clear color for color buffer (RGBA, black)
	glViewport(0,0,win->Xres,win->Yres);				// zone de rendu (tout l'�cran)
 	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LESS);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// mapping quality = best
	glFrontFace(GL_CCW);								// front of face is defined counter clock wise
	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK,  GL_LINE);					// back of a face is made of lines
	glCullFace(GL_BACK);								// cull back face only
	glDisable(GL_CULL_FACE);						    // disable back face culling


	win->set_title("Mon jeu");

	/////////////////// TP2 //////////////////
	texture_arbre=new Texture();
	
	/////////////////// TP2 Exercice 1.1 //////////////////
	texture_arbre->load_texture("texture_arbre.tga",NULL);

	/////////////////// TP2 Exercice 1.2 //////////////////	
	texture_arbre->load_texture("texture_arbre.tga","arbre_masque.tga");

	/////////////////// TP2 Exercice 1.1 et 1.2 //////////////////	
	glGenTextures(1, texture_arbre->OpenGL_ID);				// cr�e un "nom" de texture (un identifiant associ� � la texture)


	/////////////////// TP2 Exercice 2.1 //////////////////	
	texture_logo = new Texture();
	texture_logo->load_texture("artemis.tga", "artemis_mask.tga");


	///////////////// TP2 bonus - la foret
	positions_arbres_X = new float[DIM_FOREST];
	positions_arbres_Z = new float[DIM_FOREST];	
	for (int i = 0; i< DIM_FOREST; i++){
		positions_arbres_X[i] = SIZE_LAND * (2* rand() - RAND_MAX )/ RAND_MAX;
		positions_arbres_Z[i] = SIZE_LAND * (2* rand() - RAND_MAX) / RAND_MAX;
	}
		
	/////////////////// TP2 Exercice 3.1 //////////////////	
	geometrie_raptor= new Model3D();
	geometrie_raptor->load_3d_model("RAPTOR.off");
	texture_raptor=new Texture();
	texture_raptor->load_texture("texture_raptor.tga",NULL);
	glGenTextures(1, texture_raptor->OpenGL_ID);				// cr�e un "nom" de texture (un identifiant associ� � la texture)

	geometrie_frog = new Model3D();
	geometrie_frog->load_3d_model("icosphere.obj");


	/////////////////// TP3 Exercice 1.1 //////////////////	
	heightmap = new Image();
	heightmap->load_tga("heightmap.tga");
	texture_terrain= new Texture();
	texture_terrain->load_texture("texture_terrain.tga",NULL);
	glGenTextures(1, texture_arbre->OpenGL_ID);				// cr�e un "nom" de texture (un identifiant associ� � la texture)

	for (int i = 0; i < APPLE_COUNT; i++)
	{
		inactiveApple[i] = false;
	}

	return true;
}

/********************************************************************\
*                                                                    *
*  les formes geometriques                                              *
*                                                                    *
\********************************************************************/

/////////////////// TP1 Exercice 3.1 //////////////////	
void display_pyramide(float x, float y, float z, float h, float c){

	glBegin(GL_TRIANGLES);
		glColor3f(0, 1, 0); glVertex3f(x + c / 2, y, z + c / 2);
		glColor3f(0,1,0); glVertex3f(x,y+h,z);		
		glColor3f(1, 0, 0); glVertex3f(x - c / 2, y, z + c / 2);

		glColor3f(0,1,0); glVertex3f(x-c/2,y, z+c/2);
		glColor3f(0,1,0); glVertex3f(x,y+h,z);		
		glColor3f(1,0,0); glVertex3f(x-c/2,y, z-c/2);

		glColor3f(1,0,0); glVertex3f(x-c/2,y, z-c/2);
		glColor3f(0,1,0); glVertex3f(x,y+h,z);		
		glColor3f(0,1,0); glVertex3f(x+c/2,y, z-c/2);

		glColor3f(1,0,0); glVertex3f(x+c/2,y, z-c/2);
		glColor3f(0,1,0); glVertex3f(x,y+h,z);		
		glColor3f(0,1,0); glVertex3f(x+c/2,y, z+c/2);
	glEnd();

	  glBegin(GL_QUADS);
		glColor3f(0, 1, 0); glVertex3f(x + c / 2, y, z+ c / 2);
		glColor3f(1, 0, 0); glVertex3f(x - c / 2, y, z+ c / 2);
		glColor3f(1, 0, 0); glVertex3f(x - c / 2, y, z - c / 2);
		glColor3f(0, 1, 0); glVertex3f(x + c / 2, y, z- c / 2);
	  glEnd();
}

/////////////////// TP1 Exercice 3.1 //////////////////	
////////////////// pyramide qui peut tourner autour de soi m�me //////////
void display_pyramide(float h, float c){

	glBegin(GL_TRIANGLES);
		glColor3f(0, 1, 0); glVertex3f(c / 2, 0,  c / 2);
		glColor3f(0, 1, 0); glVertex3f(0, h, 0);
		glColor3f(1, 0, 0); glVertex3f(- c / 2, 0,  c / 2);

		glColor3f(0, 1, 0); glVertex3f(- c / 2, 0,  c / 2);
		glColor3f(0, 1, 0); glVertex3f(0,  h, 0);
		glColor3f(1, 0, 0); glVertex3f(- c / 2, 0,  - c / 2);

		glColor3f(1, 0, 0); glVertex3f(- c / 2, 0,  - c / 2);
		glColor3f(0, 1, 0); glVertex3f(0,  h, 0);
		glColor3f(0, 1, 0); glVertex3f(c / 2, 0,  - c / 2);

		glColor3f(1, 0, 0); glVertex3f(c / 2, 0, - c / 2);
		glColor3f(0, 1, 0); glVertex3f(0,  h, 0);
		glColor3f(0, 1, 0); glVertex3f(c / 2, 0,  c / 2);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(0, 1, 0); glVertex3f(c / 2, 0,  c / 2);
		glColor3f(1, 0, 0); glVertex3f(- c / 2, 0,  c / 2);
		glColor3f(1, 0, 0); glVertex3f(- c / 2, 0, -c / 2);
		glColor3f(0, 1, 0); glVertex3f(+ c / 2, 0, - c / 2);
	glEnd();
}


/////////////////// TP1 Exercice 4.1 //////////////////	
////////////////// l'horloge //////////
void display_horloge(){
	
	glPushMatrix();
	float heure=(float)tim->get_heure();
	float angleH = heure/12*360;
	glRotatef(angleH,0,0,1);
	glColor3f(1,1,0); 
	glBegin(GL_TRIANGLES);
		glVertex3f(0.3,0, 0);
		glVertex3f(-0.3,0,0);
		glVertex3f(0,7,0);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	float minute=(float)tim->get_minute();
	float angleM = minute/60*360;
	glRotatef(angleM,0,0,1);
	glBegin(GL_TRIANGLES);
		glColor3f(1,0,1); 
		glVertex3f(0.3,-0.3, 0);
		glVertex3f(-0.3,0.3, 0);
		glVertex3f(0,9,0);
		glEnd();
	glPopMatrix();
		
	glPushMatrix();
	float seconde=(float)tim->get_seconde();
	float angleS = seconde/60*360;
	glRotatef(angleS,0,0,1);
	glBegin(GL_LINES);
		glColor3f(1,0.5,0.5); 
		glVertex3f(0,0, 0);
		glVertex3f(0,6.75, 0);
		glEnd();
	glPopMatrix();

	int i;
	for(i=0;i<12;i++){
		glPushMatrix();
			glRotatef(30*i,0,0,1);
			glTranslatef(0,10,0);
			if(i%3==0)
				glScalef(1,1,1);
			else glScalef(0.5,0.5,1);
			glBegin(GL_QUADS);
				glColor3f(1,0,0);
				glVertex3f(-0.5,-1, 0);
				glVertex3f(-0.5,1, 0);
				glVertex3f(0.5,1, 0);
				glVertex3f(0.5,-1, 0);
			glEnd();	  
		glPopMatrix();
	}
}

/////////////////// end TP1 Exercice 4.1 //////////////////	




/////////////////// TP2 Exercice 1.1 et 1.2 //////////////////	
void display_arbre()
{
	
	glBindTexture(GL_TEXTURE_2D, texture_arbre->OpenGL_ID[0]);	// et on active ce "nom" comme texture courante (d�finie plus bas)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap
	
	if (texture_arbre->isRGBA)
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_arbre->img_color->lenx, texture_arbre->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_arbre->img_all);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8,  texture_arbre->img_color->lenx, texture_arbre->img_color->leny, GL_RGB,  GL_UNSIGNED_BYTE, texture_arbre->img_color->data);
	

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.4); // on utilise cette fonction pour controller la transparence dans l'exercice 1.2 de TP 2
		
	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK,  GL_FILL);		
		
	glBegin(GL_QUADS);
		 glTexCoord2f(0,0); glVertex3f(-5,0, 0);
		 glTexCoord2f(0,1); glVertex3f(-5,20, 0);
		 glTexCoord2f(1,1); glVertex3f(5,20, 0);
		 glTexCoord2f(1,0); glVertex3f(5,0, 0);
	glEnd();

	glPushMatrix();
		glRotatef(90, 0, 1, 0);
		glBegin(GL_QUADS);
			 glTexCoord2f(0,0); glVertex3f(-5,0, 0);
			 glTexCoord2f(0,1); glVertex3f(-5,20, 0);
			 glTexCoord2f(1,1); glVertex3f(5,20, 0);
			 glTexCoord2f(1,0); glVertex3f(5,0, 0);
		glEnd();
	glPopMatrix();

	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK,  GL_LINE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
}
/////////////////// end TP2 Exercice 1.1 et 1.2 //////////////////	


///////////////////  TP2 Exercice 2.1 //////////////////
void display_sprite(){

	glRasterPos2i(75,-55);
	glPixelZoom(0.4, 0.4);
	glDisable(GL_DEPTH_TEST);

	if (texture_logo->isRGBA)
		glDrawPixels(texture_logo->img_color->lenx, texture_logo->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_logo->img_all);
	else 
		glDrawPixels(texture_logo->img_color->lenx, texture_logo->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_logo->img_color->data);
	glEnable(GL_DEPTH_TEST);
}
///////////////////  TP2 end Exercice 2.1 //////////////////
float diff(int x, int y)
{
	float ax = abs(x);
	float ay = abs(y);

	if (ax > ay) 
	{
		return ax - ay;
	}
	else
	{
		return ay - ax;
	}
}

bool isNear(coord appleCoord, coord playerCoord)
{
	if ((diff(appleCoord.x, playerCoord.x) < 20) && (diff(appleCoord.y, playerCoord.y) < 20) && (diff(appleCoord.z, playerCoord.z) < 20))
	{
		return true;
	}
	return false;
}
///////////////////  TP2 bonus //////////////////
void display_optimized_forest(coord playerCoord)
{

	glBindTexture(GL_TEXTURE_2D, texture_arbre->OpenGL_ID[0]);	// et on active ce "nom" comme texture courante (d�finie plus bas)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	if (texture_arbre->isRGBA)
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_arbre->img_color->lenx, texture_arbre->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_arbre->img_all);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_arbre->img_color->lenx, texture_arbre->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_arbre->img_color->data);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.4); // on utilise cette fonction pour controller la transparence dans l'exercice 1.2 de TP 2

	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_FILL);

	for (int ii = 0; ii < APPLE_COUNT; ii++){
		glPushMatrix();
		//glTranslatef(positions_arbres_X[i], O, positions_arbres_Z[i]);

		// quand le terrain n'est pas plat, on doit positioner les arbres sur le terrain ... donc la position sur Y depend de l'hauteur du terrain
		float I = positions_arbres_X[ii] / horisontal_resolution;
		float J = positions_arbres_Z[ii] / horisontal_resolution;
		int i = (int)I + (int)heightmap->lenx / 2;
		int j = (int)J + (int)heightmap->leny / 2;
		
		float pp = ((float)heightmap->data[3 * (i + heightmap->lenx*(j + 0))] +
			(float)heightmap->data[3 * (i + heightmap->lenx*(j + 1))] +
			(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 0))] +
			(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 1))]) / 4;

		float Y_arbre = vertical_scale*pp;
		// end calcul positionement sur Y

		glTranslatef(positions_arbres_X[ii], Y_arbre, positions_arbres_Z[ii]);
		coord appleCoord = {positions_arbres_X[ii], Y_arbre, positions_arbres_Z[ii] };
		if (isNear(appleCoord, playerCoord))
		{
			inactiveApple[ii] = true;
		}
		if(!inactiveApple[ii])
		{
			glBegin(GL_QUADS);
				glTexCoord2f(0, 0); glVertex3f(-5, 0, 0);
				glTexCoord2f(0, 1); glVertex3f(-5, 20, 0);
				glTexCoord2f(1, 1); glVertex3f(5, 20, 0);
				glTexCoord2f(1, 0); glVertex3f(5, 0, 0);
			glEnd();
			glPushMatrix();
				glRotatef(appleRotation, 0, 1, 0);
				glBegin(GL_QUADS);
					glTexCoord2f(0, 0); glVertex3f(-5, 0, 0);
					glTexCoord2f(0, 1); glVertex3f(-5, 20, 0);
					glTexCoord2f(1, 1); glVertex3f(5, 20, 0);
					glTexCoord2f(1, 0); glVertex3f(5, 0, 0);
				glEnd();
			glPopMatrix();
		}
		glPopMatrix();
	}

	glPolygonMode(GL_FRONT, GL_FILL);					// front of a face is filled
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
}
/////////////////// TP2 end bonus //////////////////	


/////////////////// TP2 exercice 3.1 le raptor //////////////////	
void display_raptor()
{
	glBindTexture(GL_TEXTURE_2D,texture_raptor->OpenGL_ID[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	if (texture_raptor->isRGBA)
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_raptor->img_all);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_raptor->img_color->data);

	glEnable(GL_TEXTURE_2D);

	glPushMatrix();

	glTranslatef(0, 14, 0); // pour le positioner sur le terrain
	glRotatef(90,1,0,0);
	glRotatef(180,0,1,0);	
	glScalef(0.1,0.1,0.1);	


	glBegin(GL_TRIANGLES);
		for(int i=0;i<geometrie_raptor->nb_triangles;i++){
			glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].a].ucol,geometrie_raptor->points[geometrie_raptor->faces[i].a].vcol);			
			glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].a].x,geometrie_raptor->points[geometrie_raptor->faces[i].a].y,geometrie_raptor->points[geometrie_raptor->faces[i].a].z);
			
			glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].b].ucol,geometrie_raptor->points[geometrie_raptor->faces[i].b].vcol);
			glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].b].x,geometrie_raptor->points[geometrie_raptor->faces[i].b].y,geometrie_raptor->points[geometrie_raptor->faces[i].b].z);
			
			glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].c].ucol,geometrie_raptor->points[geometrie_raptor->faces[i].c].vcol);
			glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].c].x,geometrie_raptor->points[geometrie_raptor->faces[i].c].y,geometrie_raptor->points[geometrie_raptor->faces[i].c].z);
		}
	 glEnd();
	 glPopMatrix();
	 glDisable(GL_TEXTURE_2D);
}
/////////////////// TP2 end exercice 3.1 le raptor //////////////////	
/////////////////// TP2 exercice 3.1 le raptor //////////////////	
void display_frog()
{
	glBindTexture(GL_TEXTURE_2D, texture_raptor->OpenGL_ID[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmapdisplay_raptor

	if (texture_raptor->isRGBA)
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_raptor->img_all);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_raptor->img_color->data);

	glEnable(GL_TEXTURE_2D);

	glPushMatrix();

	glTranslatef(0, 14, 0); // pour le positioner sur le terrain
	glRotatef(90, 1, 0, 0);
	glRotatef(180, 0, 1, 0);
	glScalef(0.1, 0.1, 0.1);


	glBegin(GL_TRIANGLES);
	for (int i = 0; i < geometrie_frog->nb_triangles; i++) {
		glTexCoord2f(geometrie_frog->points[geometrie_frog->faces[i].a].ucol, geometrie_frog->points[geometrie_frog->faces[i].a].vcol);
		glVertex3f(geometrie_frog->points[geometrie_frog->faces[i].a].x, geometrie_frog->points[geometrie_frog->faces[i].a].y, geometrie_frog->points[geometrie_frog->faces[i].a].z);

		glTexCoord2f(geometrie_frog->points[geometrie_frog->faces[i].b].ucol, geometrie_frog->points[geometrie_frog->faces[i].b].vcol);
		glVertex3f(geometrie_frog->points[geometrie_frog->faces[i].b].x, geometrie_frog->points[geometrie_frog->faces[i].b].y, geometrie_frog->points[geometrie_frog->faces[i].b].z);

		glTexCoord2f(geometrie_frog->points[geometrie_frog->faces[i].c].ucol, geometrie_frog->points[geometrie_frog->faces[i].c].vcol);
		glVertex3f(geometrie_frog->points[geometrie_frog->faces[i].c].x, geometrie_frog->points[geometrie_frog->faces[i].c].y, geometrie_frog->points[geometrie_frog->faces[i].c].z);
	}
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void display_raptor(float x, float y, float z, float angleX, float angleY)
{
	glBindTexture(GL_TEXTURE_2D, texture_raptor->OpenGL_ID[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	if (texture_raptor->isRGBA)
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_raptor->img_all);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_raptor->img_color->lenx, texture_raptor->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_raptor->img_color->data);

	glEnable(GL_TEXTURE_2D);

	glPushMatrix();

	glTranslatef(x, y, z); // pour le positioner sur le terrain
	glRotatef(-90, 1, 0, 0);
	glRotatef(angleY, 0, 0, 1);
	glScalef(0.1, 0.1, 0.1);


	glBegin(GL_TRIANGLES);
	for (int i = 0; i < geometrie_raptor->nb_triangles; i++) {
		glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].a].ucol, geometrie_raptor->points[geometrie_raptor->faces[i].a].vcol);
		glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].a].x, geometrie_raptor->points[geometrie_raptor->faces[i].a].y, geometrie_raptor->points[geometrie_raptor->faces[i].a].z);

		glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].b].ucol, geometrie_raptor->points[geometrie_raptor->faces[i].b].vcol);
		glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].b].x, geometrie_raptor->points[geometrie_raptor->faces[i].b].y, geometrie_raptor->points[geometrie_raptor->faces[i].b].z);

		glTexCoord2f(geometrie_raptor->points[geometrie_raptor->faces[i].c].ucol, geometrie_raptor->points[geometrie_raptor->faces[i].c].vcol);
		glVertex3f(geometrie_raptor->points[geometrie_raptor->faces[i].c].x, geometrie_raptor->points[geometrie_raptor->faces[i].c].y, geometrie_raptor->points[geometrie_raptor->faces[i].c].z);
	}
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}




/////////////////// TP3 exercice 1.1 le terrain //////////////////	
void display_terrain()
{

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture_terrain->OpenGL_ID[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// on r�p�te la texture en cas de U,V > 1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// ou < 0.0
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // indique qu'il faut m�langer la texture avec la couleur courante

	// charge le tableau de la texture en m�moire vid�o et cr�e une texture mipmap

	if (texture_terrain->isRGBA)
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, texture_terrain->img_color->lenx, texture_terrain->img_color->leny, GL_RGBA, GL_UNSIGNED_BYTE, texture_terrain->img_all);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texture_terrain->img_color->lenx, texture_terrain->img_color->leny, GL_RGB, GL_UNSIGNED_BYTE, texture_terrain->img_color->data);

	glBegin(GL_TRIANGLES);
	for (int i=0;i<(int)heightmap->lenx-1;i++)
	{
		for (int j=0;j<(int)heightmap->leny-1;j++)
		{
			float I = i-(int)heightmap->lenx/2;
			float J = j-(int)heightmap->leny/2;

			glTexCoord2f((float)i/(float)heightmap->lenx,(float)j/(float)heightmap->leny);
			glVertex3f((I+0)*horisontal_resolution, vertical_scale*(float)heightmap->data[3*(i+  heightmap->lenx*(j+0))],(J+0)*horisontal_resolution);

			glTexCoord2f((float)i/(float)heightmap->lenx,(float)(j+1)/(float)heightmap->leny);
			glVertex3f((I+0)*horisontal_resolution, vertical_scale*(float)heightmap->data[3*(i+  heightmap->lenx*(j+1))],(J+1)*horisontal_resolution);

			glTexCoord2f((float)(i+1)/(float)heightmap->lenx,(float)j/(float)heightmap->leny);
			glVertex3f((I+1)*horisontal_resolution, vertical_scale*(float)heightmap->data[3*(i+1+heightmap->lenx*(j+0))],(J+0)*horisontal_resolution);

			glTexCoord2f((float)i/(float)heightmap->lenx,(float)(j+1)/(float)heightmap->leny);
			glVertex3f((I+0)*horisontal_resolution, vertical_scale*(float)heightmap->data[3*(i+  heightmap->lenx*(j+1))],(J+1)*horisontal_resolution);

			glTexCoord2f((float)(i+1)/(float)heightmap->lenx,(float)(j+1)/(float)heightmap->leny);
			glVertex3f((I+1)*horisontal_resolution, vertical_scale*(float)heightmap->data[3*(i+1+heightmap->lenx*(j+1))],(J+1)*horisontal_resolution);

			glTexCoord2f((float)(i+1)/(float)heightmap->lenx,(float)j/(float)heightmap->leny);
			glVertex3f((I+1)*horisontal_resolution, vertical_scale*(float)heightmap->data[3*(i+1+heightmap->lenx*(j+0))],(J+0)*horisontal_resolution);
		}
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);

}
/////////////////// TP3 end exercice 1.1 le terrain //////////////////	


/********************************************************************\
*                                                                    *
*  Boucle principale, appel�e pour construire une image, g�rer les   *
*  entr�es.                                                          *
*                                                                    *
\********************************************************************/
void main_loop()
{

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//				gestion des touches	et du temps													//
	//////////////////////////////////////////////////////////////////////////////////////////////////

	inp->refresh();
	tim->update_horloge();
	inp->get_mouse_movement();


	if (inp->keys[KEY_CODE_ESCAPE])
	{
		PostMessage(win->handle, WM_CLOSE, 0, 0);	// Stoppe la "pompe � message" en y envoyant le message "QUIT"
	}


	if (tim->global_timer_25_Hz)				// augmente incrementAngleY tous les 20�me de seconde
		incrementAngleY += 1.0f;
	if (incrementAngleY >= 360) incrementAngleY -= 360;





	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	//////////////////////////////////////////////////////////////////////////////////////////////////
	//						�a commence ici															//
	//////////////////////////////////////////////////////////////////////////////////////////////////

	// votre code OpenGL ici


// TP1 exercice 1.1
	if (inp->keys[KEY_CODE_SPACE])
		glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
	else
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// end TP1 exercice 1.1


	glMatrixMode(GL_PROJECTION);  //la matrice de projection sera celle selectionnee
	//composition d'une matrice de projection
	glLoadIdentity(); //on choisit la matrice identit�
	gluPerspective(60, (double)win->Xres / (double)win->Yres, 1, 10000);   //mise en place d une proj angle de vue 60 deg near 10 far 30000

	// fonction � utiliser pour tous les exercices sauf le 2.1 du TP3: gluLookAt a une position fixe
	//	gluLookAt(0, 60, -180,		// position
	//		  0, 50, 1,		// point cible
	//		  0, 1, 0);		// vecteur up


//////////////// TP3 exercice 2.1 controle de la camera

	point dir = cam->direction - cam->position;
	if (inp->keys[KEY_CODE_UP]) {
		posX += dir.x * playerSpeed;
		posZ += dir.z * playerSpeed;
		playerAngleY = 180;
	}
	if (inp->keys[KEY_CODE_DOWN]) {
		posX -= dir.x * playerSpeed;
		posZ -= dir.z * playerSpeed;
		playerAngleY = 0;
	}


	if (inp->keys[KEY_CODE_RIGHT]) {
		posX += produit_vectoriel(dir, cam->orientation).x * playerSpeed;
		posZ += produit_vectoriel(dir, cam->orientation).z * playerSpeed;
		playerAngleY = 90;
	}
	if (inp->keys[KEY_CODE_LEFT]) {
		posX -= produit_vectoriel(dir, cam->orientation).x * playerSpeed;
		posZ -= produit_vectoriel(dir, cam->orientation).z * playerSpeed;
		playerAngleY = 270;
	}

	if (raptorCounter <= 0) 
	{
		raptorCounter = rand() % 100;
		raptorXInfluence = 0;
		raptorYInfluence = 0;

		if (raptorCounter < 25)
		{
			raptorXInfluence = 1*raptorSpeed;
			raptorAngleY = 90;
		}
		else if (raptorCounter < 50)
		{
			raptorXInfluence = -1 * raptorSpeed;
			raptorAngleY = 270;
		}
		else if (raptorCounter < 75)
		{
			raptorYInfluence = 1 * raptorSpeed;
			raptorAngleY = 0;
		}
		else
		{
			raptorYInfluence = -1 * raptorSpeed;
			raptorAngleY = 180;
		}

	}
	raptorCounter--;




	// Dev tool
	if (inp->keys[KEY_CODE_A]) {
		xInfluence += 10;
		printf("xInfluence: %d", xInfluence);
	}
	if (inp->keys[KEY_CODE_Z]) {
		xInfluence -= 10;
		printf("xInfluence: %d", xInfluence);
	}
	if (inp->keys[KEY_CODE_E]) {
		zInfluence += 10;
		printf("zInfluence: %d", zInfluence);
	}
	if (inp->keys[KEY_CODE_R]) {
		zInfluence -= 10;
		printf("zInfluence: %d", zInfluence);
	}

	appleRotation += 8;

	angleX -= 0.1*(float)inp->Yrelmouse;
	angleY -= 0.1*(float)inp->Xrelmouse;
	if (angleY > 360) angleY -= 360; if (angleY < 0) angleY += 360;
	if (angleX > 60) angleX = 60;	if (angleX < -60) angleX = -60;

	float I = posX / horisontal_resolution;
	float J = posZ / horisontal_resolution;
	int i = (int)I + (int)heightmap->lenx / 2;
	int j = (int)J + (int)heightmap->leny / 2;

	float pp = ((float)heightmap->data[3 * (i + heightmap->lenx*(j + 0))] +
		(float)heightmap->data[3 * (i + heightmap->lenx*(j + 1))] +
		(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 0))] +
		(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 1))]) / 4;

	posY = 10 + vertical_scale * pp; // 

	I = raptorPosX / horisontal_resolution;
	J = raptorPosZ / horisontal_resolution;
	i = (int)I + (int)heightmap->lenx / 2;
	j = (int)J + (int)heightmap->leny / 2;

	pp = ((float)heightmap->data[3 * (i + heightmap->lenx*(j + 0))] +
		(float)heightmap->data[3 * (i + heightmap->lenx*(j + 1))] +
		(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 0))] +
		(float)heightmap->data[3 * (i + 1 + heightmap->lenx*(j + 1))]) / 4;

	raptorPosY = 10 + vertical_scale * pp; //

	coord playerPos = { posX, posY, posZ };
	coord enemyPos = { raptorPosX, raptorPosY, raptorPosZ };

	// GAME OVER
	if (isNear(enemyPos, playerPos))
	{
		posX = 0;
		posY = 30;
		posZ = 0;
		angleX = 0;
		angleY = 0;
		angleZ = 0;
	}


	//cam->update(point(posX,posY, posZ),angleX,angleY,angleZ);
	cam->update(point(posX + xInfluence, 100, posZ + zInfluence + 90), -45, 0, angleZ);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cam->position.x, cam->position.y, cam->position.z,		// position
		cam->direction.x, cam->direction.y, cam->direction.z,		// point cible
		cam->orientation.x, cam->orientation.y, cam->orientation.z);		// vecteur up

//////////////// TP3 end exercice 2.1 controle de la camera

	write_2_screen("Welcome to the OpenGL Laboratory");

	/////////////////////////// TP1 ////////////////////

		////////// TP1: exercice 3.1 les pyramides ////////////
			//glPushMatrix();
			//glRotatef(incrementAngleY, 0, 1, 0);
			//display_pyramide(30, 30, 0, 50, 20);
			//glPopMatrix();

			//glPushMatrix();
			//glTranslatef(30, 30, 0);
			//glRotatef(incrementAngleY, 0, 1, 0);
			//display_pyramide(50, 20); // cette pyramide peut tourner autour de soi-m�me
			//glPopMatrix();
		////////// TP1:end exercice 3.1 les pyramides ////////////

		////////// TP1:exercice 4.1 l'horloge ////////////
			//display_horloge();
		////////// TP1:exercice 4.1 l'horloge ////////////

	///////////////////////////// TP2 ////////////////////

		////////// TP2: exercice 1.1 et 1.2 l'arbre ////////////
			//display_arbre();
		////////// TP2: end exercice 1.1 et 1.2 l'arbre ////////

		////////// TP2: exercice 2.1 le sprite 2D//////////
			//display_sprite();
		////////// TP2: exercice 2.1 le sprite 2D//////////
	//////////// TP2: bonus - optimized forest
	display_optimized_forest(playerPos);
	//////////// TP2: end bonus - optimized forest

	raptorPosX += raptorXInfluence;
	raptorPosZ += raptorYInfluence;
	//////////// TP2: exercice 3.1 le raptor
	display_raptor(raptorPosX, raptorPosY, raptorPosZ, raptorAngleX, raptorAngleY);
	//////////// TP2: end exercice 3.1 le raptor

	//////////// TP3: exercice 1.1 le terrain
	display_terrain();
	//////////// TP3: end exercice 1.1 le terrain
	display_raptor(posX, posY, posZ, angleX, playerAngleY);

	display_frog();

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//						�a finit ici															//
	//////////////////////////////////////////////////////////////////////////////////////////////////
	swap_buffer(win);	// affiche l'image compos�e � l'�cran
}


/********************************************************************\
*                                                                    *
* Arr�te l'application                                               *
*                                                                    *
\********************************************************************/
void stop()
{

	delete geometrie_raptor;
	delete texture_arbre;
	delete texture_raptor;
	delete texture_logo;
	delete positions_arbres_X;
	delete positions_arbres_Z;

	delete heightmap;
	delete texture_terrain;
	delete cam;	
	delete inp;
	delete tim;

	if (win)
	{
		kill_font();
		kill_context(*win);
		delete win;
	}
}




/********************************************************************\
*                                                                    *
* Point d'entr�e de notre programme pour Windows ("WIN32")           *
*                                                                    *
\********************************************************************/
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR szCmdLine,int iCmdShow)
{
	MSG  msg;

	win	= NULL;
	inp	= NULL;
	tim = NULL;

	if (start() == false)								// initialise la fen�tre et OpenGL
	{
		debug("start() : �chec !");
		stop();
		return 1;
	}

	// main loop //
	// --------- //
	//   __
	//  /  \_
	//  |  \/ 
	//  \__/

	while (true)
	{
		if (PeekMessage(&msg, NULL,0,0,PM_NOREMOVE))	// s'il y a un message, appelle WndProc() pour le traiter
		{
			if (!GetMessage(&msg,NULL,0,0))				// "pompe � message"
				break;
			TranslateMessage(&msg);
			DispatchMessage (&msg);
		}
		else 
		{
			main_loop();								// sinon, appelle main_loop()
		}
	}


	stop();												// arr�te OpenGL et ferme la fen�tre

	return 0;
}



/********************************************************************\
*                                                                    *
*  Boucle des messages                                               *
*                                                                    *
\********************************************************************/
LRESULT CALLBACK WndProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{

  switch (msg)
    {

      case WM_MOVE:        win->Xpos = (dword) LOWORD(lParam);
                           win->Ypos = (dword) HIWORD(lParam);
                           return 0;

      case WM_CLOSE:       PostQuitMessage(0); // dit � GetMessage() de renvoyer 0
                           return 0;

      case WM_SYSCOMMAND:  // �vite l'�conomiseur d'�cran
                           switch (wParam)
                             {
                               case SC_SCREENSAVE:
                               case SC_MONITORPOWER:
                               return 0;
                             }
                           break;
/*
      case WM_CHAR:        la touche est traduite dans ce msg 
                           return 0;
*/

      case WM_KEYDOWN:     inp->set_key_down ((dword)wParam);
                           return 0;

      case WM_KEYUP:       inp->set_key_up   ((dword)wParam);
                           return 0;

      case WM_LBUTTONDOWN: inp->set_mouse_left_down();
                           return 0;

      case WM_LBUTTONUP:   inp->set_mouse_left_up();
                           return 0;

      case WM_RBUTTONDOWN: inp->set_mouse_right_down();
                           return 0;

      case WM_RBUTTONUP:   inp->set_mouse_right_up();
                           return 0;

      case WM_MBUTTONDOWN: inp->set_mouse_middle_down();
                           return 0;

      case WM_MBUTTONUP:   inp->set_mouse_middle_up();
                           return 0;

   }

  return DefWindowProc(hwnd,msg,wParam,lParam);
}

