//
// Created by trotfunky on 18/10/2019.
//

#ifndef TESTS_OPENGL_QUATERNION_H
#define TESTS_OPENGL_QUATERNION_H

#include "Vectors.h"

struct Quaternion : CoordinatesVector<double,4>{
    double& x = coordinates[0];
    double& y = coordinates[1];
    double& z = coordinates[2];
    double& w = coordinates[3];

    Quaternion() = default;
    Quaternion(double axis_x, double axis_y, double axis_z, double w_value);
    Quaternion(const Quaternion& original);
    Quaternion(const CoordinatesVector<double,4>& original);
    Quaternion(const CoordinatesVector<double,3>& original);

    /// Returns the inverse of a *unit-quaternion*.
    Quaternion inverse() const;

    /// Quaterion multiplication. See also https://en.wikipedia.org/wiki/Quaternion#Hamilton_product
    Quaternion operator*(const Quaternion& op) const;

    /// Performs the rotation associated to the *unit-quaternion* on the Vec3 operand.
    Vec3d friend operator*(const Quaternion& rot, const Vec3d& op);
    Vec3d friend operator*(const Vec3d& op, const Quaternion& rot);

    Quaternion& operator=(const Quaternion& original);

    /// Cast operator
    explicit operator Vec3d() const;
};


#endif //TESTS_OPENGL_QUATERNION_H
