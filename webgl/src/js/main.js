import * as THREE from '../../node_modules/three/build/three.module.js';
import { GLTFLoader } from '../../node_modules/three/examples/jsm/loaders/GLTFLoader.js';
import { TGALoader } from '../../node_modules/three/examples/jsm/loaders/TGALoader.js';
import { OrbitControls } from '../../node_modules/three/examples/jsm/controls/OrbitControls.js';
import { Clock } from '../../node_modules/three/src/core/Clock.js';


// Make the img global so we can easily access the width and height.
var img;

// How much to scale the height of the heightfield.
var height_scale = 90;

// raptor movement variables
var raptorX = 0;
var raptorZ = 0;
var raptorSpeed = 0.2;
var raptorMoveTimer = 0;

// player movement variables
const ySpeed = 3;
const xSpeed = 3;

var terrainWidth = 45;
var terrainHeight = 45;

var apples = [];

var apple_count = 4;

var geometry;

const proximity = 3;

let fox, player, plane;

var score = 0;


window.onload = (event) => {

    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    

    const renderer = init();
    const clock = new Clock({autoStart: true});
    load_ground(); // all functions are chained in the callback -> load_ground -> load_light -> load_raptor -> load_player -> load_apples
    
    var controls = new OrbitControls( camera, renderer.domElement );
    
    render();


    function render() {
        renderer.render( scene, camera );
    }
    
    function init() {
        
        const renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( renderer.domElement );
        
        camera.position.z = 15;
        camera.position.y = 12;
        camera.lookAt (new THREE.Vector3(0,-3,0));
        
        return renderer;
    }

    //To get the pixels, draw the image onto a canvas. From the canvas get the Pixel (R,G,B,A)
    function getTerrainPixelData()
    {
        img = document.getElementById("landscape-image");
        var canvas = document.getElementById("canvas");
        
        canvas.width = img.width;
        canvas.height = img.height;
        canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);

        var data = canvas.getContext('2d').getImageData(0,0, img.width, img.height).data;
        var normPixels = [];

        for (var i = 0, n = data.length; i < n; i += 4) {
            // get the average value of R, G and B.
            normPixels.push((data[i] + data[i+1] + data[i+2]) / 3);
        }

        return normPixels;
    }

    function load_ground() {
        var terrain = getTerrainPixelData();  
      
        //var geometry = new THREE.PlaneGeometry(2400, 2400*img.width/img.height, img.height-1, img.width-1);
        geometry = new THREE.PlaneGeometry(2400*img.width/img.height, 2400, img.width-1, img.height-1);
        console.log({"geometry": geometry});
        var material = new THREE.MeshLambertMaterial({
          color: 0xccccff,
          wireframe: true
        });
      
        // keep in mind, that the plane has more vertices than segments. If there's one segment, there's two vertices, if
        // there's 10 segments, there's 11 vertices, and so forth. 
        // The simplest is, if like here you have 100 segments, the image to have 101 pixels. You don't have to worry about
        // "skewing the landscape" then..
      
        // to check uncomment the next line, numbers should be equal
        console.log("length: " + terrain.length + ", vertices length: " + geometry.vertices.length);
      
        for (var i = 0, l = geometry.vertices.length; i < l; i++)
        {
          var terrainValue = terrain[i] / 255;
          geometry.vertices[i].z = geometry.vertices[i].z + terrainValue * height_scale ;
        }
        
        // might as well free up the input data at this point, or I should say let garbage collection know we're done.
        terrain = null;
      
        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
      
        const tga_loader = new TGALoader();
        
        
        //camera.lookAt(plane);
        // load a resource
        const texture = tga_loader.load('src/assets/tga/texture_terrain.tga',
        // called when loading is completed
        function ( texture ) {
            console.log( 'Texture is loaded' );
            scene.background = new THREE.Color(0,43,69);
            const tga_material = new THREE.MeshPhongMaterial( { color: 0xffffff, map: texture } );
            //var planeGeo = new THREE.PlaneGeometry( 20, 20, 129, 129 );
            //var tga_plane = new THREE.Mesh(	planeGeo, tga_material );
            //tga_plane.rotation.x = -0.5 * Math.PI;
            //tga_plane.receiveShadow = true;
            //tga_plane.position.y = -5;
            
            plane = new THREE.Mesh(geometry, tga_material);
            
            //plane.position = new THREE.Vector3(0,0,0);
            //plane.position.x = 0;
            //plane.position.z = 0;
            //plane.position.y = 0;
            // rotate the plane so up is where y is growing..
            
            var q = new THREE.Quaternion();
            q.setFromAxisAngle( new THREE.Vector3(-1,0,0), 90 * Math.PI / 180 );
            plane.quaternion.multiplyQuaternions( q, plane.quaternion );
            
            //plane.receiveShadow = true;
            //plane.rotation.x = -0.5 * Math.PI;
            //plane.receiveShadow = true;
            plane.position.y = 0;
            
            var scalar = terrainWidth / geometry.parameters.width;

            console.log("terrainWidth:" + terrainWidth + " / geometry.parameters.width: " + geometry.parameters.width + "so scalar is: " + scalar)
            console.log({"plane:":plane});
            plane.scale.multiplyScalar(scalar);
                
            //camera.lookAt(plane);
                scene.add(plane);
                load_light();
                //scene.add(tga_plane);
                return plane;
            },
            // called when the loading is in progresses
            function ( xhr ) {console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );},
            // called when the loading failes
            function ( error ) {console.log( 'An error happened' ); }
        );
        
    }

    function load_light() {
        let hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.61);
        hemiLight.position.set(0, 50, 0);
        scene.add(hemiLight);


        load_raptor();
            //let floorGeo = new THREE.PlaneGeometry(5000, 5000, 1, 1);
            //let floorMat = new THREE.MeshPhongMaterial({color: 0xeeeeee,shininess: 0});

            //let floor = new THREE.Mesh(floorGeo, floorMat);
            //floor.rotation.x = -0.5 * Math.PI;
            //floor.receiveShadow = true;
            //floor.position.y = -11;
            //scene.add(floor);
    }

    function load_raptor() {     
        //const loader = new GLTFLoader().setPath( 'src/assets/gltf/Fox/RAPTORCopy.glb' );
        const loader = new GLTFLoader();
        loader.load( 'src/assets/gltf/RAPTORCopy.glb', function ( gltf ) {

            gltf.scene.traverse( function ( child ) {

                if ( child.isMesh ) {

                    // TOFIX RoughnessMipmapper seems to be broken with WebGL 2.0
                    // roughnessMipmapper.generateMipmaps( child.material );
                    if(child instanceof THREE.Mesh){
                        child.material.color = new THREE.Color(0X00FF00);
                        child.geometry.computeVertexNormals();
                    }
                }

            } );
            const tga = new TGALoader();
            tga.load('src/assets/tga/texture_raptor.tga',
             function(raptor_texture){
                
             },
                // called when the loading is in progresses
            function ( xhr ) {console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );},
            // called when the loading failes
            function ( error ) {console.log( 'An error happened' ); }
            );
            fox = gltf.scene;
            fox.scale.multiplyScalar(0.03);
            fox.position.x = -5;
            fox.position.z = -5;
            fox.rotation.x = 1.5 * Math.PI;
            //fox.rotation.y = 5;
            //fox.up = new THREE.Vector3(1,0,0);
            fox.receiveShadow = true;
            //fox.position.y = 1;
            scene.add( fox );
            load_player();
            return fox;

            //roughnessMipmapper.dispose();

            render();

        } );
    }

    function load_player() {     
        const loader = new GLTFLoader().setPath( 'src/assets/gltf/Duck/glTF/' );
        loader.load( 'Duck.gltf', function ( gltf ) {

            gltf.scene.traverse( function ( child ) {

                if ( child.isMesh ) {

                    // TOFIX RoughnessMipmapper seems to be broken with WebGL 2.0
                    // roughnessMipmapper.generateMipmaps( child.material );
                    if(child instanceof THREE.Mesh){
                        child.material.color = new THREE.Color(0X00FF00);
                        child.geometry.computeVertexNormals();
                    }
                }

            } );

            player = gltf.scene;

            player.position.y = 1;

            player.receiveShadow = true;

            controls.target = player.position;

            camera.position.x = player.position.x;
            camera.position.z = player.position.z + 10;
            scene.add( player );
            load_apples();
            document.addEventListener("keydown", onDocumentKeyDown, false);
            return player;

            //roughnessMipmapper.dispose();
        } );
    }

    function reset_game(){

        var point;
        for(var i =  0; i < apple_count; i++) {
            point = choose_apple_location({width:terrainWidth, height:terrainHeight});
            apples[i].position.x = point.x;
            apples[i].position.z = point.z;
            apples[i].visible = true;

        }


        player.position.x = 0;
        player.position.z = 0;

        fox.position.x = -5;
        fox.position.z = -5;
    }

    function load_cube(){
        const geometry = new THREE.BoxGeometry();
        const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        const cube = new THREE.Mesh( geometry, material );
        scene.add( cube );
        return cube;
    }

    function load_apples(){
        const tga_loader = new TGALoader();

        // load a resource
        const texture = tga_loader.load('src/assets/tga/texture_arbre.tga',
            // called when loading is completed
            function ( texture ) {console.log( 'Texture is loaded' );
                const mask = tga_loader.load('src/assets/tga/arbre_masque.tga',
                function(texture_mask){
                    console.log('texture is loaded');
                    const tga_material = new THREE.MeshBasicMaterial( { color: 0xffffff, map: texture, alphaMap:  texture_mask, transparent: true} );
                    console.log({"tga_material":tga_material});
                    tga_material.side =  THREE.DoubleSide;
                    var planeGeo = new THREE.PlaneGeometry( 2, 2, 2, 2 );
                    
                    
                    var apple, point;

                    for(var i =  0; i < apple_count; i++) {
                        apple = new THREE.Mesh(planeGeo, tga_material );
                        point = choose_apple_location({width:terrainWidth, height:terrainHeight});
                        apple.position.x = point.x;
                        apple.position.z = point.z;
                        apple.position.y = 3;
                        
                        apples.push(apple);
                        scene.add(apple);
                    }
                    animate();
                    //console.log({"apple3":apple3});
                },
                
                    // called when the loading is in progresses
                    function ( xhr ) {console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );},
                    // called when the loading failes
                    function ( error ) {console.log( 'An error happened' ); }
                );
                
                //return tga_plane;
            },
            // called when the loading is in progresses
            function ( xhr ) {console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );},
            // called when the loading failes
            function ( error ) {console.log( 'An error happened' ); }
        );

    }

   
    //if ( WEBGL.isWebGLAvailable() ) {

   
   

   
        //var texture = THREE.ImageUtils.loadTexture('src/assets/tga/texture_terrain.tga', null, loaded);

    
        
    function choose_raptor_movement(){


        if(raptorMoveTimer == 0) {
            // wait 1 seconds to move again + another 1 to 5 seconds
            raptorMoveTimer = 60 + getRandomInt(300); 


            if(raptorMoveTimer % 2 == 0) {
                raptorZ = 0;
                if((getRandomInt(2) == 1)) {
                    raptorX = 1;
                    fox.rotation.z = 0.5 * Math.PI;
                }
                else {
                    raptorX = -1;
                    fox.rotation.z = -0.5 * Math.PI;
                }
            }
            else {
                raptorX = 0;
                if((getRandomInt(2) == 1)) {
                    raptorZ = 1;
                    fox.rotation.z = 0 * Math.PI;
                }
                else {
                    raptorZ = -1;
                    fox.rotation.z = 1 * Math.PI;
                }
            }
        }

        raptorMoveTimer--;

        if(is_out_of_bounds(fox.position, {width: terrainWidth, length: terrainHeight})
            || is_underwater({x: (fox.position.x / terrainWidth),z: (fox.position.z / terrainWidth)}, geometry)){
            // go in the opposite direction
            raptorX = raptorX * -1;
            raptorZ = raptorZ * -1;
            fox.rotation.z += Math.PI;
            
        }

        fox.position.x += raptorSpeed * raptorX;
        fox.position.z += raptorSpeed * raptorZ;


        if(player.position.distanceTo(fox.position) < proximity) {
            score = 0;
            document.getElementById("info").innerHTML = "GAME OVER";

            reset_game();
        }
    }

    function animate() {
        requestAnimationFrame( animate );


        //cube.position.x = (cube.position.x + 0.01) % 4;
        if(fox != undefined) {
            choose_raptor_movement();
            //fox.rotation.z += 0.02;
        }

        //if(apple1 != undefined) apple1.rotation.y += 0.02;
        //if(apple2 != undefined) apple2.rotation.y += 0.02;
        //if(apple3 != undefined) apple3.rotation.y += 0.02;

        for(var i =  0; i < apple_count; i++) {
            apples[i].rotation.y += 0.02
            if(player.position.distanceTo(apples[i].position) < proximity) {
                apples[i].visible = false;
                apples[i].position.z = 5000;
                score++;
                document.getElementById("info").innerHTML = "Score: " + score;
                if(score >= apple_count) {
                    document.getElementById("info").innerHTML = "Duck Mode!!!";
                    reset_game();
                }
            }
        }
        //fox.position.x = (fox.position.x + 0.01) % 4;
        //camera.rotation.x += 0.01;
        //console.log(camera.rotation.x);
        render();
    }
    
   
    function onDocumentKeyDown(event) {
        var keyCode = event.which;
        console.log(keyCode);
        var saveX = player.position.x;
        var saveZ = player.position.z;
        if (keyCode == 40) { // down
            player.position.z += ySpeed * clock.getDelta();
            player.rotation.y = -0.5 * Math.PI;
        } else if (keyCode == 38) { // up
            player.position.z -= ySpeed * clock.getDelta();
            player.rotation.y = 0.5 * Math.PI;
        } else if (keyCode == 39) { // right
            player.position.x += xSpeed * clock.getDelta();
            player.rotation.y = 0 * Math.PI;
        } else if (keyCode == 37) { // left
            player.position.x -= xSpeed * clock.getDelta();
            player.rotation.y = 1 * Math.PI;
        }

        if(is_out_of_bounds(player.position, {width: terrainWidth, length: terrainHeight})) {
            player.position.x = saveX;
            player.position.z = saveZ;
        }

        camera.position.x = player.position.x;
        camera.position.z = player.position.z + 10;
        // else if (keyCode == 32) {
        //    cube.position.set(0, 0, 0);
        //}
    };

    /*} else {

        const warning = WEBGL.getWebGLErrorMessage();
        document.getElementById( 'container' ).appendChild( warning );

    }*/


};

