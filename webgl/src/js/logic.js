


const depth = 12;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function is_underwater(model_pos, geo) {

    return calculate_z_position(model_pos, geo) < depth;
}

// given the mesh array, find out where model 
// should be on the z (up) axis
// /!\ model_pos is in percentages
function calculate_z_position(model_pos, geo) {
    
    var scaledY = Math.floor( (255/2)+ model_pos.z * 250);
    var scaledX = Math.floor( (255/2)+ model_pos.z * 250);

    //console.log({"scaled Y:": scaledY, "scaledX:":scaledX});
    var z = geo.vertices[scaledY * 250 + scaledX].z;

    return z;
}


function is_out_of_bounds(model_pos, heightmap) {
    if (model_pos.x < -heightmap.width / 2
        || model_pos.x > heightmap.width / 2
        || model_pos.z < -heightmap.length / 2
        || model_pos.z > heightmap.length / 2){
           // console.log({"model_pos:":model_pos,"heightmap":heightmap});
            return true;
        }

    return false;
}

function choose_apple_location(heightmap){

    //model_pos.y = calculate_z_position(model_pos, heightmap)
    return {x : getRandomInt(heightmap.width) - (heightmap.width / 2),
        z: getRandomInt(heightmap.height) - (heightmap.width / 2)};
}

