# OpenGL raptor game 


## Disclaimer

Only the code in main.cpp was written by me.

## Building

Just type 

```bash
make
```

and then 

```bash
./a.out
```

And a blank window should open

### Dependencies

The following libraries must be installed:
	- glfw (can be installed with $ brew install glfw3)
  
